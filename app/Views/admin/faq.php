
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/content_management">Faq Management</a>
				</li>
			</ul><!-- /.breadcrumb -->

		</div>

		<div class="page-content">
			<div class="page-header">
				<h1>
					Faq List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#add_faq">Add Faq </div>	
			</div>

			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="add_faq" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Add Faq
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      <span aria-hidden="true">×</span>
			                    </button>
		                    </h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>addFaq">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Question * </label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Question" class="col-xs-10 col-sm-5" name="question" required="" />
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Answer * </label>

											<div class="col-sm-9">
												<textarea id="form-field-11" class="autosize-transition form-control" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 52px;" placeholder="Answer" name="answer" required=""></textarea>
											</div>
										</div>

										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												<!-- 
												&nbsp; &nbsp; &nbsp;
												<button class="submit" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button> -->
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
<!------------------------  Modal for Edit Content --------------------------->
			<?php
				foreach($faq_details as $content){ 
			?>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="edit_content<?=$content['faq_id']?>" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Edit Faq
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      <span aria-hidden="true">×</span>
			                    </button>
		                    </h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php base_url(); ?>editFaq/<?=$content['faq_id']?>">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Question * </label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Question" class="col-xs-10 col-sm-5" name="question" required="" value="<?= $content['question'] ?>" />
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Answer * </label>

											<div class="col-sm-9">
												<textarea id="form-field-11" class="autosize-transition form-control" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 52px;" placeholder="Answer" name="answer" required=""><?= $content['answer']?></textarea>
											</div>
										</div>

										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												<!-- 
												&nbsp; &nbsp; &nbsp;
												<button class="submit" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button> -->
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
<!---------------------------- Content List ---------------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Question</th>
								<th scope="col">Answer</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($faq_details as $content){ 

								$snum += 1;
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $content['question']?></td>
								<td><?= $content['answer']?></td>
								<td>
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120" data-toggle="modal" data-target="#edit_content<?=$content['faq_id']?>"></i>
									</span>
									
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
