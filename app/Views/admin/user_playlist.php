<?php
use App\Models\Playlist_songs;
$playlist_songs = new Playlist_songs();
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/user_playlist_management/<?=$user_id;?>">User Playlist Management</a>
				</li>
			</ul>
		</div>
		<div class="page-content">
			<div class="page-header">
				<h1>
					User Playlist
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#add_playlist">Create Playlist </div>
			</div>
			<!----------------  Modal for Create Playlist ----------------------->
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="add_playlist" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Create Playlist
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      <span aria-hidden="true">×</span>
			                    </button>
		                  	</h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/addUserPlaylist" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Playlist name *</label>
											<div class="col-sm-9">
												<input type="hidden" name="user_id" value="<?=$user_id;?>">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-8" name="playlist_name" required="" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Banner Image*</label>
											<div class="col-sm-9">
												<input type="file" id="form-field-1" class="col-xs-10 col-sm-8" name="banner_image" required="" />
											</div>
										</div>
										
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		<!------------------- User Playlist--------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Playlist Name</th>
								<th scope="col">Banner Image</th>
								<th scope="col">Songs</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$snum = 0;
							foreach($playlist_details as $playlist){ 
								$snum += 1;
								$playlist_song_data = count($playlist_songs->crud_read('', $playlist['user_playlist_id']));
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $playlist['playlist_name']?></td>
								<td>
									<img src="<?= base_url()."/writable/uploads/".$playlist['banner_image']?>" height="100px" width="100px" alt="Banner Image">
								</td>
								<td>
									<a href="<?php echo base_url(); ?>/admin/playlist_song_management/<?=$playlist['user_playlist_id']?>">
										<span style="margin-left: 40px;"><?php echo $playlist_song_data;?></span>
									</a>
								</td>
								<td>
									<a href="<?php echo base_url(); ?>/admin/deleteUserPlaylist/<?=$playlist['user_playlist_id']?>/<?=$user_id?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
