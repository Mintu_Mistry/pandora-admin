<?php
use App\Models\User_playlist;
$user_playlist = new User_playlist();

use App\Models\User_songs;
$user_songs = new User_songs();
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/users_management">User Management</a>
				</li>
			</ul>
		</div>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Users List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#add_user">Add User </div>
			</div>
			<!----------------  Modal for User ----------------------->
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="add_user" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Add User
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      <span aria-hidden="true">×</span>
			                    </button>
		                  	</h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/addUser" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Full Name *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="full_name" required="" placeholder="Full Name" pattern="^[A-Za-z]\w*$" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> User Email *</label>
											<div class="col-sm-9">
												<input type="email" id="form-field-1" class="col-xs-10 col-sm-5" name="user_email" required="" placeholder="User Email" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> User Mobile *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="mobile" required="" placeholder="User Mobile" onkeypress="return isNumber(event)" minlength="10" maxlength="10" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Profile Image *</label>
											<div class="col-sm-9">
												<input type="file" id="form-field-1" class="col-xs-10 col-sm-8" name="profile_image" required="" accept="image/*"/>
											</div>
										</div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!----------------  Modal for edit user ----------------------->
			<?php
				foreach($users_details as $user){ 
			?>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="edit_user<?=$user['user_id']?>" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Edit User
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      <span aria-hidden="true">×</span>
			                    </button>
			                </h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/editUser/<?=$user['user_id']?>" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Full Name *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="full_name" required="" placeholder="Full Name" value="<?= $user['full_name']?>" pattern="^[A-Za-z]\w*$"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> User Email *</label>
											<div class="col-sm-9">
												<input type="email" id="form-field-1" class="col-xs-10 col-sm-5" name="user_email" required="" placeholder="User Email" value="<?= $user['user_email']?>" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> User Mobile *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="mobile" required="" placeholder="User Mobile" value="<?= $user['mobile']?>" onkeypress="return isNumber(event)" minlength="10" maxlength="10"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Profile Image *</label>
											<div class="col-sm-9">
												<input type="file" id="form-field-1" class="col-xs-10 col-sm-8" name="profile_image" accept="image/*"/>
											</div>
										</div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
		<!------------------- Users List --------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Full Name</th>
								<th scope="col">User Email</th>
								<th scope="col">User Mobile</th>
								<th scope="col">Profile Image</th>
								<!-- <th scope="col">User Songs</th> -->
								<th scope="col">User Playlist</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($users_details as $user){ 
								$snum += 1;
								$playlist_data = count($user_playlist->crud_read('', $user['user_id']));
								$user_song_data = count($user_songs->crud_read('', $user['user_id']));
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $user['full_name']?></td>
								<td><?= $user['user_email']?></td>
								<td><?= $user['mobile']?></td>
								<td>
									<?php 
										if(!empty($user['is_social'] == 'url')){
											$profile_image = $user['profile_image'];
										}
										else {
											$profile_image = base_url()."/writable/uploads/".$user['profile_image'];
										}
									?>
									<img src="<?= $profile_image?>" height="100px" width="100px" alt="Profile Image">
								</td>
								<!-- <td>
									<a href="<?php base_url(); ?>user_songs_management/<?=$user['user_id']?>" target="blank">
										<span style="margin-left: 40px;"><?php echo $user_song_data;?></span>
									</a>
								</td> -->
								<td>
									<a href="<?php base_url(); ?>user_playlist_management/<?=$user['user_id']?>" target="blank">
										<span style="margin-left: 40px;"><?php echo $playlist_data;?></span>
									</a>
								</td>
								<td>
									<!-- <span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120" data-toggle="modal" data-target="#edit_user<?=$user['user_id']?>"></i>
									</span> -->
									<a href="<?php base_url(); ?>deleteUser/<?=$user['user_id']?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
								
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function isNumber(evt) {
	    evt = (evt) ? evt : window.event;
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	        alert("Enter numbers only");
	        return false;
	    }
	    return true;
	}
</script>