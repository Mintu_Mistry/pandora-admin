<ul class="nav nav-list">
	<li class="active">
		<a href="<?php echo base_url(); ?>">
			<i class="menu-icon fa fa-tachometer"></i>
			<span class="menu-text"> Dashboard </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/artist_management">
			<i class="menu-icon fa fa-user"></i>
			<span class="menu-text"> Artist Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/contact_management">
			<i class="menu-icon fa fa-envelope"></i>
			<span class="menu-text"> Contact Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/content_management">
			<i class="menu-icon glyphicon glyphicon-align-left"></i>
			<span class="menu-text"> Content Management </span>
		</a>
		<b class="arrow"></b>
	</li>	
	
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/faq_management">
			<i class="menu-icon glyphicon glyphicon-align-left"></i>
			<span class="menu-text"> Faq Management </span>
		</a>
		<b class="arrow"></b>
	</li>	
	
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/feedback_management">
			<i class="menu-icon fa fa-commenting"></i>
			<span class="menu-text"> Feedback Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/songs_management">
			<i class="menu-icon fa fa-music"></i>
			<span class="menu-text"> Songs Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/stations_management">
			<i class="menu-icon fa fa-film"></i>
			<span class="menu-text"> Stations Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/subscription_management">
			<i class="menu-icon fa fa-money"></i>
			<span class="menu-text"> Subscription Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/top10_songs_management">
			<i class="menu-icon fa fa-asterisk"></i>
			<span class="menu-text"> Top 10 Songs Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/trending_songs_management">
			<i class="menu-icon fa fa-star"></i>
			<span class="menu-text"> Trending Songs Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/users_management">
			<i class="menu-icon fa fa-users"></i>
			<span class="menu-text"> User Management </span>
		</a>
		<b class="arrow"></b>
	</li>
</ul>