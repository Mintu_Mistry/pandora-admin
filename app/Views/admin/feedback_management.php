
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/feedback_management">Feedback Management</a>
				</li>
			</ul><!-- /.breadcrumb -->

		</div>

		<div class="page-content">
			<div class="page-header">
				<h1>
					Feedback List
				</h1>
			</div>
<!---------------------------- Feedback List ---------------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Rating</th>
								<th scope="col">Email</th>
								<th scope="col">Message</th>
								<th scope="col">Date</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($feedback_details as $feedback){ 
								$snum += 1;
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $feedback['rating']?></td>
								<td><?= $feedback['email']?></td>
								<td><?= $feedback['message']?></td>
								<td><?= date('m-d-Y', strtotime($feedback['created_at']))?></td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
