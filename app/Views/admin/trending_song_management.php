<?php
use App\Models\Songs;
$trending = new Songs();
?>

<div class="main-content">
	<div class="main-content-inner">
		<style>
			/* Chrome, Safari, Edge, Opera */
			input::-webkit-outer-spin-button,
			input::-webkit-inner-spin-button {
			  -webkit-appearance: none;
			  margin: 0;
			}

			/* Firefox */
			input[type=number] {
			  -moz-appearance: textfield;
			}
		</style>
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/trending_songs_management">Trending Songs Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Trending Songs List
				</h1>	
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#updateorder">Update Trending Song Order</div>
			</div>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="updateorder" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Update Order 
								<button style="float:right;" type="button" class="close" data-dismiss="modal" aria-label="Close">
			                     	<span aria-hidden="true">×</span>
			                    </button>
		                    </h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/updateTrendingOrder" enctype="multipart/form-data">
										<table class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th scope="col">S.No</th>
													<th scope="col">Song Name</th>
													<th scope="col">Song Image</th>
													<th scope="col">Song Order</th>
												</tr>
											</thead>
											<tbody>
												<?php 
												$snum = 0;
												uasort($trending_song_details, function ($i, $j) {
												    $a = $i['trending_song_order'];
												    $b = $j['trending_song_order'];
												    if ($a == $b) return 0;
												    elseif ($a > $b) return 1;
												    else return -1;
												});

												foreach($trending_song_details as $song){
													$snum += 1;
													$order_number = $trending->crud_read($song['songs_id']);
													/*if($song['play_count'] == 0)
													continue;*/
												?>
												<tr>
													<th scope="row"><?= $snum?></th>
													<td><?= $song['song_name']?></td>
													
													<td><img src="<?php echo base_url()."/writable/uploads/".$song['thumbnail']?>" height="100px" width="100px" alt="Thumbnail"></td>
													<td>
														<input type="hidden" name="songid[]" value="<?= $song['songs_id']?>" required="">
														<input type="number" min="1" max="10" name="song_order[]" required="" value="<?=$order_number[0]['trending_song_order']?>">
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<!------------------------- Songs List ------------------------------>
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Song Name</th>
								<th scope="col">Song Track</th>
								<th scope="col">Song Image</th>
								<!-- <th scope="col">Action</th> -->
							</tr>
						</thead>
						<tbody>

							<?php 
							uasort($trending_song_details, function ($i, $j) {
							    $a = $i['trending_song_order'];
							    $b = $j['trending_song_order'];
							    if ($a == $b) return 0;
							    elseif ($a > $b) return 1;
							    else return -1;
							});
							$snum = 0;
							foreach($trending_song_details as $song){ 
								$snum += 1;
								/*if($song['play_count'] == 0)
								continue;*/
								//$song = $songs->crud_read($trending_song['songid']);
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $song['song_name']?></td>
								<td><a href="<?= base_url()."/writable/uploads/".$song['song_track']?>" target="blank" ><?= $song['song_track']?></a></td>
								<td><img src="<?php echo base_url()."/writable/uploads/".$song['thumbnail']?>" height="100px" width="100px" alt="Thumbnail"></td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function isNumber(evt) {
	    evt = (evt) ? evt : window.event;
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	        alert("Enter numbers only");
	        return false;
	    }
	    return true;
	}
</script>