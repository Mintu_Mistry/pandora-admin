<?php
use App\Models\Artist_songs;
$artist_songs = new Artist_songs();

use App\Models\Artist_album;
$artist_album = new Artist_album();
?>

<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/users_management">Artist Management</a>
				</li>
			</ul>
		</div>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Artists List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#add_artist">Add Artist </div>
			</div>
			<!----------------  Modal for Add Artist ----------------------->
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="add_artist" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Add Artist
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      <span aria-hidden="true">×</span>
			                    </button>
		                  	</h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/addArtist" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Banner Image*</label>
											<div class="col-sm-9">
												<input type="file" id="form-field-1" class="col-xs-10 col-sm-8" name="banner_image" required="" accept=".jpeg,.png"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Cover Image *</label>
											<div class="col-sm-9">
												<input type="file" id="form-field-1" class="col-xs-10 col-sm-8" name="cover_image" required=""  accept=".jpeg,.png"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Artist Name *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="artist_name" required="" placeholder="Artist Name" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Artist Genre *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="artist_genre" required="" placeholder="Artist Genre" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Description *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="description" required="" placeholder="Description" />
											</div>
										</div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!----------------  Modal for Edit artist ----------------------->
			<?php
				foreach($artist_details as $artist){ 
			?>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="edit_artist<?=$artist['artist_id']?>" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Edit Artist
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      <span aria-hidden="true">×</span>
			                    </button>
			                </h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/editArtist/<?=$artist['artist_id']?>" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Banner Image*</label>
											<div class="col-sm-9">
												<input type="file" id="form-field-1" class="col-xs-10 col-sm-8" name="banner_image" accept=".jpeg,.png"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Cover Image *</label>
											<div class="col-sm-9">
												<input type="file" id="form-field-1" class="col-xs-10 col-sm-8" name="cover_image" accept=".jpeg,.png"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Artist Name *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="artist_name" required="" placeholder="Artist Name" value="<?= $artist['artist_name']?>"  />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Artist Genre *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="artist_genre" required="" placeholder="Artist Genre" value="<?= $artist['artist_genre']?>" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Description *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="description" required="" placeholder="Description" value="<?= $artist['description']?>" />
											</div>
										</div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
		<!------------------- Artist List --------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Artist Name</th>
								<th scope="col">Artist Genre</th>
								<th scope="col">Banner Image</th>
								<th scope="col">Cover Image</th>
								<th scope="col">Description</th>
								<th scope="col">Songs</th>
								<th scope="col">Albums</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($artist_details as $artist){ 
								$snum += 1;
								$artist_song_data = count($artist_songs->crud_read('', $artist['artist_id']));
								$artist_album_data = count($artist_album->crud_read('', $artist['artist_id']));
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $artist['artist_name']?></td>
								<td><?= $artist['artist_genre']?></td>
								<td>
									<img src="<?= base_url()."/writable/uploads/".$artist['banner_image']?>" height="100px" width="100px" alt="Profile Image">
								</td>
								<td>
									<img src="<?= base_url()."/writable/uploads/".$artist['cover_image']?>" height="100px" width="100px" alt="Profile Image">
								</td>
								<td><?= $artist['description']?></td>
								<td>
									<a href="<?php base_url(); ?>artist_songs_management/<?=$artist['artist_id']?>" target="blank" >
										<span style="margin-left: 40px;"><?php echo $artist_song_data;?></span>
									</a>
								</td>
								<td>
									<a href="<?php base_url(); ?>artist_album_management/<?=$artist['artist_id']?>" target="blank">
										<span style="margin-left: 40px;"><?php echo $artist_album_data;?></span>
									</a>
								</td>
								<td>
									<!-- <span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120" data-toggle="modal" data-target="#edit_artist<?=$artist['artist_id']?>"></i>
									</span> -->
									<a href="<?php base_url(); ?>deleteArtist/<?=$artist['artist_id']?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
