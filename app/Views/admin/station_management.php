<?php
use App\Models\Station_songs;
$station_songs = new Station_songs();
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/stations_management">Station Management</a>
				</li>
			</ul>
		</div>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Stations List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#add_station">Create Station </div>
			</div>
			<!----------------  Modal for Create Station ----------------------->
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="add_station" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Create Station
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      <span aria-hidden="true">×</span>
			                    </button>
		                  	</h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/addStation" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Station name *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-8" placeholder="Station name" name="station_name" required="" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Station Genre *</label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Station genre" class="col-xs-10 col-sm-5" name="station_genre" required="" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Banner Image*</label>
											<div class="col-sm-9">
												<input type="file" id="form-field-1" class="col-xs-10 col-sm-8" name="banner_image" required="" accept=".jpeg,.png" />
											</div>
										</div>
										
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		<!------------------- Stations List--------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Station Name</th>
								<th scope="col">Station Genre</th>
								<th scope="col">Banner Image</th>
								<th scope="col">Songs</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$snum = 0;
							foreach($stations_details as $stations){ 
								$snum += 1;
								$station_song_data = count($station_songs->crud_read('', $stations['station_id']));
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $stations['station_name']?></td>
								<td><?= $stations['station_genre']?></td>
								<td>
									<img src="<?= base_url()."/writable/uploads/".$stations['banner_image']?>" height="100px" width="100px" alt="Banner Image">
								</td>
								<td>
									<a href="<?php echo base_url(); ?>/admin/station_songs_management/<?=$stations['station_id']?>" target="blank">
										<span style="margin-left: 40px;"><?php echo $station_song_data;?></span>
									</a>
								</td>
								<td>
									<a href="<?php echo base_url(); ?>/admin/deleteStation/<?=$stations['station_id']?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
