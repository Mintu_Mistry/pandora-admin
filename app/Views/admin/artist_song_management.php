<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/artist_songs_management/<?=$artist_id;?>">Artist Songs Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Songs List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#addsong">Add Song </div>
			</div>
			<!------------- Modal for Add Song ------------------>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="addsong" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Add Song 
								<button style="float:right;" type="button" class="close" data-dismiss="modal" aria-label="Close">
			                     	<span aria-hidden="true">×</span>
			                    </button>
		                    </h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/addArtistSong" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Song Name *</label>

											<div class="col-sm-9">
												<input type="hidden" name="artistid" value="<?= $artist_id?>">
												<input type="text" id="form-field-1" placeholder="Song name" class="col-xs-10 col-sm-5" name="song_name" required="" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Song Genre *</label>

											<div class="col-sm-9">
												
												<input type="text" id="form-field-1" placeholder="Song Genre" class="col-xs-10 col-sm-5" name="song_genre" required="" />
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Song Track *</label>

											<div class="col-sm-9">
												<input type="file" name="song_track" class="file-input" accept=".mp3,audio/*" required="">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Upload Image *</label>

											<div class="col-sm-9">
												<input type="file" name="song_image" class="file-input" accept=".jpeg,.png" required="">
											</div>
										</div>
										<!-- <div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Top 10 *</label>

											<div class="col-sm-9">
												<input type="checkbox" name="top_10" class="file-input" value="1">
											</div>
										</div> -->
										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
<!------------------------- Songs List ------------------------------>
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Song Name</th>
								<th scope="col">Song Genre</th>
								<th scope="col">Song Track</th>
								<th scope="col">Song Image</th>
								<!-- <th scope="col">Top 10</th> -->
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$snum = 0;
							foreach($artist_song_details as $song){ 
								$snum += 1;
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $song['song_name']?></td>
								<td><?= $song['song_genre']?></td>
								<td><a href="<?= base_url()."/writable/uploads/".$song['song_track']?>" target="blank" ><?= $song['song_track']?></a></td>
								<td><img src="<?php echo base_url()."/writable/uploads/".$song['thumbnail']?>" height="100px" width="100px" alt="Thumbnail"></td>
								<!-- <td>
									<?php if($song['top_10'] == 0){?>
										<span class="label label-sm label-danger">NO</span>
									<?php } else{ ?>
										<span class="label label-sm label-success">YES</span>
									<?php } ?>
								</td> -->
								<td>
									<a href="<?php echo base_url(); ?>/admin/deleteArtistSong/<?=$song['artist_song_id']?>/<?=$artist_id?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
