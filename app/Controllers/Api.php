<?php 
/*

	Pandora API controller

*/

namespace App\Controllers;

//require FCPATH . 'vendor/autoload.php';
require_once APPPATH .'ThirdParty/twilio-php-main/src/Twilio/autoload.php';
use App\Models\Login;
use App\Models\Content;
use App\Models\Contact;
use App\Models\Feedback;
use App\Models\Artist;
use App\Models\Artist_songs;
use App\Models\Artist_album;
use App\Models\Songs;
use App\Models\User_playlist;
use App\Models\Playlist_songs;
use App\Models\Users;
use App\Models\Recent_search;
use App\Models\Top_10_songs;
use App\Models\Trending_songs;
use App\Models\Subscription;
use App\Models\Stations;
use App\Models\Station_songs;
use App\Models\User_songs;
use App\Models\Artist_album_song;
use App\Models\Download_song;
use App\Models\Faq;
use App\Models\Favorite_song;


use CodeIgniter\HTTP\Files\UploadedFile;
use CodeIgniter\API\ResponseTrait;
use Twilio\Rest\Client;

class Api extends BaseController

{
	use ResponseTrait;
	private $TWILIO_SID = 'AC38a3e19d11936ea109e611e140b784b4';
	private	$TWILIO_TOKEN = '829b962d6e0ec8034b5d06e40e9e7ae8';


	private function apiResponse($data)
	{
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function index()
	{
		$returndata = array(
			"message" => "Pandora API"
		);
		return $this->apiResponse($returndata);
	}

	private function sendTwilioSMS($text,$to)
	{
		try{
			$twilio = new Client($this->TWILIO_SID, $this->TWILIO_TOKEN);
	        return $twilio->messages->create(
			    $to,
			    [
			        'from' => '+12513366477',
			        'body' => $text
			    ]
			);
		}
		catch(Exception $e){
			return 0;
		}
		finally{
			return 0;
		}		
	}

	public function get_content()
	{
		$returndata = array();
		$content = new Content();
		$content_details = $content->crud_read();
		
		if($content_details){
			$returndata['status'] = TRUE;
			$returndata['data'] = $content_details;
			$returndata['message'] = "Content fetched successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}
	
	public function submit_contact_query()
	{
		$returndata = array();
		$contact = new Contact();
		$user_name = $this->request->getPost('user_name');
		$email = $this->request->getPost('email');
		$message = $this->request->getPost('message');
		if(!empty($user_name) && !empty($email) && !empty($message)){
			$contact->crud_create(array(
				'user_name' => $user_name,
				'email' => $email,
				'message' => $message,
				'created_at' => date('Y-m-d H:i:s')
			));
			$returndata['status'] = TRUE;
			$returndata['message'] = "Contact query submitted successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function submit_feedback()
	{
		$returndata = array();
		$feedback = new Feedback();
		$rating = $this->request->getPost('rating');
		$email = $this->request->getPost('email');
		$message = $this->request->getPost('message');
		if(!empty($rating) && !empty($email) && !empty($message)){
			$feedback->crud_create(array(
				'rating' => $rating,
				'email' => $email,
				'message' => $message,
				'created_at' => date('Y-m-d H:i:s')
			));
			$returndata['status'] = TRUE;
			$returndata['message'] = "Feedback submitted successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_artists_list()
	{
		$returndata = array();
		$artist = new Artist();
		$artist_details = $artist->crud_read();
		if($artist_details){
			$artist_array = array();
			foreach ($artist_details as $artist_data) {
				$artist_array[] = array(
					'artist_id' => $artist_data['artist_id'],
					'artist_name' => $artist_data['artist_name'],
					'banner_image' => base_url()."/writable/uploads/".$artist_data['banner_image'],
					'cover_image' => base_url()."/writable/uploads/".$artist_data['cover_image'],
					'description' => $artist_data['description'],
					//'top_10' => $artist_data['top_10'],
					'created_at' => date('m-d-Y', strtotime($artist_data['created_at']))
				);
			}
			$returndata['status'] = TRUE;
			$returndata['data'] = $artist_array;
			$returndata['message'] = "Artists fetched successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_artist_by_artist_id()
	{
		$returndata = array();
		$artist = new Artist();
		$artist_id = $this->request->getPost('artist_id');
		$artist_details = $artist->crud_read($artist_id);
		if($artist_details){
			$returndata['artist_data'] = array();
			$returndata['artist_data'][] = array(
				'artist_id' => $artist_details['artist_id'],
				'artist_name' => $artist_details['artist_name'],
				'banner_image' => base_url()."/writable/uploads/".$artist_details['banner_image'],
				'cover_image' => base_url()."/writable/uploads/".$artist_details['cover_image'],
				'description' => $artist_details['description'],
				'created_at' => date('m-d-Y', strtotime($artist_details['created_at']))
			);
			$returndata['similar_artists_data'] = array();
			$similar_artist_details = $artist->search_artist_by_genre($artist_details['artist_genre'], $artist_details['artist_id']);
			
			foreach ($similar_artist_details as $similar_artist) {
				$returndata['similar_artists_data'][] = array(
					'artist_id' => $similar_artist['artist_id'],
					'artist_name' => $similar_artist['artist_name'],
					'banner_image' => base_url()."/writable/uploads/".$similar_artist['banner_image'],
					'cover_image' => base_url()."/writable/uploads/".$similar_artist['cover_image'],
					'description' => $similar_artist['description'],
					'created_at' => date('m-d-Y', strtotime($similar_artist['created_at']))
				);
			}
			$returndata['status'] = TRUE;
			$returndata['message'] = "Artist details fetched successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_artist_songs()
	{
		$returndata = array();
		$artist_songs = new Artist_songs();
		$artist_id = $this->request->getPost('artist_id');
		$artist_song_details = $artist_songs->crud_read('', $artist_id);
		if(!empty($artist_id)){
			if($artist_song_details){
				$artist_song_array = array();
				foreach ($artist_song_details as $artist_data) {
					$artist_song_array[] = array(
						'artist_song_id' => $artist_data['artist_song_id'],
						'artistid' => $artist_data['artistid'],
						'song_name' => $artist_data['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$artist_data['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$artist_data['song_track'],
						//'top_10' => $artist_data['top_10'],
						'created_at' => date('m-d-Y', strtotime($artist_data['created_at']))
					);
				}
				$returndata['status'] = TRUE;
				$returndata['data'] = $artist_song_array;
				$returndata['message'] = "Artist songs fetched successfully.";
			}
			else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_artist_album()
	{
		$returndata = array();
		$artist_album = new Artist_album();
		$artist_id = $this->request->getPost('artist_id');
		$artist_album_details = $artist_album->crud_read('', $artist_id);
		if(!empty($artist_id)){
			if($artist_album_details){
				$artist_album_array = array();
				foreach ($artist_album_details as $artist_data) {
					$artist_album_array[] = array(
						'artist_album_id' => $artist_data['artist_album_id'],
						'artistid' => $artist_data['artistid'],
						'album_name' => $artist_data['album_name'],
						'thumbnail' => base_url()."/writable/uploads/".$artist_data['thumbnail'],
						//'top_10' => $artist_data['top_10'],
						'created_at' => date('m-d-Y', strtotime($artist_data['created_at']))
					);
				}
				$returndata['status'] = TRUE;
				$returndata['data'] = $artist_album_array;
				$returndata['message'] = "Artist album fetched successfully.";
			}
			else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_artist_album_songs()
	{
		$returndata = array();
		$artist_album_song = new Artist_album_song();
		$artist_albumid = $this->request->getPost('artist_albumid');
		$artist_album_song_details = $artist_album_song->crud_read('', $artist_albumid);
		if(!empty($artist_albumid)){
			if($artist_album_song_details){
				$artist_album_array = array();
				foreach ($artist_album_song_details as $song_data) {
					$artist_album_array[] = array(
						'artist_album_songs_id' => $song_data['artist_album_songs_id'],
						'artist_albumid' => $song_data['artist_albumid'],
						'song_name' => $song_data['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$song_data['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$song_data['song_track'],
						'created_at' => date('m-d-Y', strtotime($song_data['created_at']))
					);
				}
				$returndata['status'] = TRUE;
				$returndata['data'] = $artist_album_array;
				$returndata['message'] = "Artist album songs fetched successfully.";
			}
			else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	/* public function get_all_songs()
	{
		$returndata = array();
		$songs = new Songs();
		$songs_details = $songs->crud_read();
		if($songs_details){
			$songs_array = array();
			foreach ($songs_details as $song) {
				$songs_array[] = array(
					'songs_id' => $song['songs_id'],
					'song_name' => $song['song_name'],
					'thumbnail' => base_url()."/writable/uploads/".$song['thumbnail'],
					'song_artist' => $song['song_artist'],
					'song_track' => base_url()."/writable/uploads/".$song['song_track'],
					//'top_10' => $song['top_10'],
					'created_at' => date('m-d-Y', strtotime($song['created_at']))
				);
			}
			$returndata['status'] = TRUE;
			$returndata['data'] = $songs_array;
			$returndata['message'] = "Songs fetched successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	} */

	public function create_playlist()
	{
		$returndata = array();
		$user_playlist = new User_playlist();
		$user_id = $this->request->getPost("user_id");
		$playlist_name = $this->request->getPost("playlist_name");
		$banner_image = $this->request->getFile("banner_image");
		if(!empty($user_id) && !empty($playlist_name)){
			if($banner_image){
				if($banner_image->isValid()){
					$photopath = $banner_image->store();
					if($photopath != ""){
						$user_playlist->crud_create(array(
							'userid' => $user_id,
							'playlist_name' => $playlist_name,
							'banner_image' => $photopath,
							'created_at' => $created_at
						));
					}
				}
			}
			else {
				$user_playlist->crud_create(array(
					'userid' => $user_id,
					'playlist_name' => $playlist_name,
					'created_at' => $created_at
				));
			}
			$returndata['status'] = true;
        	$returndata['message'] = "Playlist created successfully!";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function delete_playlist()
	{
		$returndata = array();
		$user_playlist = new User_playlist();
		$playlist_id = $this->request->getPost("playlist_id");
		if(!empty($playlist_id)){
			$user_playlist->crud_delete($playlist_id);
			$returndata['status'] = true;
        	$returndata['message'] = "Playlist deleted successfully!";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function update_playlist()
	{
		$returndata = array();
		$user_playlist = new User_playlist();
		$playlist_id = $this->request->getPost("playlist_id");
		$playlist_name = $this->request->getPost("playlist_name");
		//$banner_image = $this->request->getFile("banner_image");
		if(!empty($playlist_id) && !empty($playlist_name)){
			$user_playlist->crud_update(array(
				'playlist_name' => $playlist_name,
				'created_at' => date('Y-m-d H:i:s')
			), $playlist_id);
			$returndata['status'] = true;
        	$returndata['message'] = "Playlist updated successfully!";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_user_playlist()
	{
		$returndata = array();
		$user_playlist = new User_playlist();
		$playlist_songs = new Playlist_songs();
		$user_id = $this->request->getPost('user_id');
		$playlist_details = $user_playlist->crud_read('', $user_id);
		if(!empty($user_id)){
			if($playlist_details){
				$playlist_array = array();
				foreach ($playlist_details as $playlist) {
					$playlist_song_data = count($playlist_songs->crud_read('', $playlist['user_playlist_id']));
					$playlist_array[] = array(
						'user_playlist_id' => $playlist['user_playlist_id'],
						'playlist_name' => $playlist['playlist_name'],
						'banner_image' => base_url()."/writable/uploads/".$playlist['banner_image'],
						'song_count' => $playlist_song_data,
						'created_at' => date('m-d-Y', strtotime($playlist['created_at']))
					);
				}
				$returndata['status'] = TRUE;
				$returndata['data'] = $playlist_array;
				$returndata['message'] = "Playlist fetched successfully.";
			}
			else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_user_profile()
	{
		$returndata = array();
		$users = new Users();
		$user_id = $this->request->getPost("user_id");
		if(!empty($user_id)){
			$user_details = $users->crud_read($user_id);
			if($user_details){
				$user_array = array();
				foreach ($user_details as $user) {
					if(!empty($user['is_social'] == 'url')){
						$profile_image = $user['profile_image'];
					}
					else {
						$profile_image = base_url()."/writable/uploads/".$user['profile_image'];
					}
					$user_array[] = array(
						'user_id' => $user['user_id'],
						'full_name' => $user['full_name'],
						'user_email' => $user['user_email'],
						'mobile' => $user['mobile'] == NULL ? '' : $user['mobile'],
						'status' => $user['status'],
						'profile_image' => $profile_image,
						'social_id' => $user['social_id'],
						'created_at' => date('m-d-Y', strtotime($user['created_at']))
					);
				}
				$returndata['status'] = true;
				$returndata['data'] = $user_array;
	        	$returndata['message'] = "User details fetched successfully!";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function set_favorite_song()
	{
		$returndata = array();
		$favorite_song = new Favorite_song();
		$user_id = $this->request->getPost('user_id');
		$song_type = $this->request->getPost('song_type');
		$song_id = $this->request->getPost('song_id');
		$fav_status = 1;
		if(!empty($user_id) && !empty($fav_status) && !empty($song_type) && !empty($song_id)){
			$fav_song_data = $favorite_song->crud_read($user_id,$song_type,$song_id);
			if(count($fav_song_data)>0){
					$returndata['status'] = true;
		        	$returndata['message'] = "This song already added to my favorite song!";
			}else{
				//fav_id	userid	song_type	track_id	fav_status	created_at
					$favorite_song->crud_create(array(
						'userid' => $user_id,
						'song_type' => $song_type,
						'track_id' => $song_id,
						'fav_status' => $fav_status,
						'created_at' => date('Y-m-d')
					));
					$returndata['status'] = true;
		        	$returndata['message'] = "Song added to my favorite song successfully!";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function remove_favorite_song()
	{
		$returndata = array();
		$favorite_song = new Favorite_song();
		$user_id = $this->request->getPost('user_id');
		$fav_id = $this->request->getPost('fav_id');
		if(!empty($user_id) && !empty($fav_id)){
			$fav_song = $favorite_song->crud_read_by_favid($user_id,$fav_id);
			if(count($fav_song)>0){
				
				$favorite_song->crud_delete($fav_id);
				$returndata['status'] = true;
		        $returndata['message'] = "This song has been removed from my favorite song list successfully!";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "This song not in my favorite song list!";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_favorite_songs()
	{
		$returndata = array();
		$songs = new Songs();
		$playlist_songs = new Playlist_songs();
		$station_songs = new Station_songs();
		$favorite_song = new Favorite_song();
		$user_id = $this->request->getPost('user_id');
		if(!empty($user_id)){
			
			$favorite_song_details = $favorite_song->crud_read($user_id);
			
			if(count($favorite_song_details)>0)
			{
				foreach($favorite_song_details as $song_dtl)
				{
					
					if($song_dtl['song_type'] ="1")
					{
						
						$song_detail = $songs->crud_read($song_id);
						$returndata['fav_songs'][] = array(
						'song_id' => $song_detail[0]['songs_id'],
						'fav_status' =>$song_dtl['fav_status'],
						'song_name' => $song_detail[0]['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$song_detail[0]['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$song_detail[0]['song_track'],
						'song_artist' => $song_detail[0]['song_artist'],
						'created_at' => date('m-d-Y', strtotime($song_detail[0]['created_at'])));
					
					}elseif($song_dtl['song_type'] == "2") {
						
						$playlist_song_detail = $playlist_songs->crud_read($song_id);
						$returndata['fav_songs'][] = array(
						'playlist_song_id' => $playlist_song_detail[0]['playlist_song_id'],
						'fav_status' =>$song_dtl['fav_status'],
						'playlistid' => $playlist_song_detail[0]['playlistid'],
						'song_name' => $playlist_song_detail[0]['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$playlist_song_detail[0]['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$playlist_song_detail[0]['song_track'],
						'favorite' => $playlist_song_detail[0]['favorite'],
						'created_at' => date('m-d-Y', strtotime($playlist_song_detail[0]['created_at'])));
					
					}elseif($song_dtl['song_type'] == "3") {
						$station_song_detail = $station_songs->crud_read($song_id);
						$returndata['fav_songs'][] = array(
						'station_song_id' => $station_song_detail[0]['station_song_id'],
						'fav_status' =>$song_dtl['fav_status'],
						'song_name' => $station_song_detail[0]['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$station_song_detail[0]['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$station_song_detail[0]['song_track'],
						'created_at' => date('m-d-Y', strtotime($station_song_detail[0]['created_at'])));
					}
				}
				
				if(!empty($returndata['fav_songs'])){
					$returndata['status'] = TRUE;
					$returndata['message'] = "Favorite songs fetched successfully.";
				}
				else{
					$returndata['status'] = FALSE;
					$returndata['message'] = "No data found.";
				}
				
			}else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_user_playlist_songs()
	{
		$returndata = array();
		$user_playlist = new User_playlist();
		$playlist_songs = new Playlist_songs();
		$playlist_id = $this->request->getPost('playlist_id');
		if(!empty($playlist_id)){
			$playlist_details = $user_playlist->crud_read($playlist_id);
			$playlist_array = array();
			foreach ($playlist_details as $playlist) {
				$playlist_array[] = array(
					'user_playlist_id' => $playlist['user_playlist_id'],
					'playlist_name' => $playlist['playlist_name'],
					'banner_image' => base_url()."/writable/uploads/".$playlist['banner_image'],
					'created_at' => date('m-d-Y', strtotime($playlist['created_at']))
				);
			}

			$playlist_songs_details = $playlist_songs->crud_read('', $playlist_id);

			$playlist_songs_array = array();
			foreach ($playlist_songs_details as $song) {
				$playlist_songs_array[] = array(
					'playlist_song_id' => $song['playlist_song_id'],
					'playlistid' => $song['playlistid'],
					'song_name' => $song['song_name'],
					'thumbnail' => base_url()."/writable/uploads/".$song['thumbnail'],
					'song_track' => base_url()."/writable/uploads/".$song['song_track'],
					'favorite' => $song['favorite'],
					'created_at' => date('m-d-Y', strtotime($song['created_at']))
				);
			}
			if(!empty($playlist_songs_array) || !empty($playlist_array)){
				$returndata['status'] = TRUE;
				$returndata['data'][] = array(
					'playlist' => $playlist_array,
					'playlist_songs' => $playlist_songs_array
				);
				$returndata['message'] = "Playlist Songs fetched successfully.";
			}
			else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_user_playlist_song_by_id()
	{
		$returndata = array();
		$playlist_songs = new Playlist_songs();
		$songs = new Songs();
		$playlist_song_id = $this->request->getPost('playlist_song_id');
		$user_id = $this->request->getPost('user_id');
		if(!empty($playlist_song_id) && !empty($user_id)){
			$playlist_songs_details = $playlist_songs->crud_read($playlist_song_id);
			if(!empty($playlist_songs_details)){
				$returndata['similar_song'] = array();
				$similar_songs_details = $songs->search_song_by_genre($playlist_songs_details[0]['song_genre']);
				foreach ($similar_songs_details as $similar_song) {
					$returndata['similar_song'][] = array(
						'songs_id' => $similar_song['songs_id'],
						'song_name' => $similar_song['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$similar_song['thumbnail'],
						'song_artist' => $similar_song['song_artist'],
						'song_track' => base_url()."/writable/uploads/".$similar_song['song_track'],
						//'top_10' => $song['top_10'],
						'created_at' => date('m-d-Y', strtotime($similar_song['created_at']))
					);
				}

				$returndata['user_song'] = array();
				$user_songs_details = $playlist_songs->read_user_song_limit($playlist_songs_details[0]['playlistid']);
				foreach ($user_songs_details as $user_song) {
					$returndata['user_song'][] = array(
						'playlist_song_id' => $user_song['playlist_song_id'],
						'playlistid' => $user_song['playlistid'],
						'song_name' => $user_song['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$user_song['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$user_song['song_track'],
						'favorite' => $user_song['favorite'],
						'created_at' => date('m-d-Y', strtotime($user_song['created_at']))
					);
				}

				$returndata['playlist_song'] = array();
				foreach ($playlist_songs_details as $song) {
					$returndata['playlist_song'][] = array(
						'playlist_song_id' => $song['playlist_song_id'],
						'playlistid' => $song['playlistid'],
						'song_name' => $song['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$song['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$song['song_track'],
						'favorite' => $song['favorite'],
						'created_at' => date('m-d-Y', strtotime($song['created_at']))
					);
				}
				
				$returndata['status'] = TRUE;
				$returndata['message'] = "Playlist Songs fetched successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "No details found.";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function login_user()
	{
		$returndata = array();
		$users = new Users();
		//echo FCPATH . 'vendor/autoload.php';die;
		//$country_code = $this->request->getPost('country_code');
		$mobile = $this->request->getPost('mobile');
		$fcm_token = $this->request->getPost('fcm_token');
		if(!empty($mobile)) {
			$user_data = $users->read_by_mobile($mobile);
			$code = mt_rand(1000, 9999);
			$message = 'Your Verification OTP Is :-'.$code;
			if(empty($user_data)){
				$result = $this->sendTwilioSMS($message, $mobile);
				$users->crud_create(array(
					'mobile'=> $mobile,
					'fcm_token'=> $fcm_token,
					'status' => 1,
					'code' => $code
				));
				$returndata['status'] = TRUE;
				$returndata['code'] = $code;
				$returndata['message'] = "Code sent successfully.";
			}
			else {
				$result = $this->sendTwilioSMS($message, $mobile);
				$users->crud_update(array(
					'fcm_token'=> $fcm_token,
					'status' => 1,
					'code' => $code
				), $user_data['user_id']);
				$returndata['status'] = TRUE;
				$returndata['code'] = $code;
				$returndata['message'] = "Code sent successfully.";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		
		return $this->apiResponse($returndata);
	}

	public function send_otp()
	{
		$returndata = array();
		$users = new Users();
		$mobile = $this->request->getPost('mobile');
		if(!empty($mobile)) {
			//$user_data = $users->read_by_mobile($mobile);
			$code = mt_rand(1000, 9999);
			$message = 'Your Verification OTP Is :-'.$code;
			/*if(empty($user_data)){*/
				$result = $this->sendTwilioSMS($message, $mobile);
				/*$users->crud_create(array(
					'mobile'=> $mobile,
					'status' => 1,
					'code' => $code
				), $user_data['user_id']);*/
				$returndata['status'] = TRUE;
				$returndata['code'] = $code;
				$returndata['message'] = "Code sent successfully.";
			/*}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "Mobile already exist.";
			}*/
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		
		return $this->apiResponse($returndata);
	}

	public function verify_otp()
	{
		$returndata = array();
		$users = new Users();
		$otp = $this->request->getPost('otp');
		$mobile = $this->request->getPost('mobile');
		$user_data = $users->read_by_mobile($mobile);
		if(!empty($otp)){
			if($user_data['code'] == $otp){
				$returndata['status'] = TRUE;
				$returndata['data'] = $user_data['user_id'];
				$returndata['message'] = "OTP verified.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "Invalid OTP.";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function social_login()
	{
		$returndata = array();
		$users = new Users();
        $social_id = $this->request->getPost('social_id');
        $full_name = $this->request->getPost('full_name');
        $user_email = $this->request->getPost('user_email');
        $fcm_token = $this->request->getPost('fcm_token');
       	$profile_image = $this->request->getPost("profile_image");
        $created_at = date('Y-m-d h:i:s');
        if (!empty($user_email) && !empty($full_name) && !empty($social_id) && !empty($fcm_token)) {
            $exist = $users->read_by_social_id($social_id);
            if(!empty($exist)){
                $exist_data['user_id'] = $exist['user_id'];
                $exist_data['fcm_token'] = $exist['fcm_token'];
                $returndata['status'] = true; 
                $returndata['data'] = $exist_data;
                $returndata['message'] = 'Logged in successfully.' ;
            }
            else {
            	/*if($profile_image){
					if($profile_image->isValid()){
						$photopath = $profile_image->store();
						if($photopath != ""){
							$user_id = $users->crud_create(array(
			                    'full_name' => $full_name,
			                    'user_email' => $user_email,
			                    'fcm_token' => $fcm_token,
			                    'social_id' => $social_id,
			                    'status' => 1,
			                    'profile_image' => $photopath,
			                    'created_at' => $created_at
			                ));
						}
					}
				}
				else {
					$user_id = $users->crud_create(array(
	                    'full_name' => $full_name,
	                    'user_email' => $user_email,
	                    'fcm_token' => $fcm_token,
	                    'social_id' => $social_id,
	                    'status' => 1,
	                    'created_at' => $created_at
	                ));
				}*/
				$user_id = $users->crud_create(array(
                    'full_name' => $full_name,
                    'user_email' => $user_email,
                    'fcm_token' => $fcm_token,
                    'social_id' => $social_id,
                    'status' => 1,
                    'profile_image' => $profile_image,
                    'is_social' => 'url',
                    'created_at' => $created_at
                ));
                $data['user_id'] = "$user_id";
                $data['fcm_token'] = $fcm_token;
                $returndata['status'] = true; 
                $returndata['data'] = $data;
                $returndata['message'] = 'Registered successfully.' ;
            }
        } else {
            $returndata['status'] = false; 
            $returndata['message'] = 'Please provide required fields.' ;
        }
        return $this->apiResponse($returndata);
    }

	public function update_user_profile()
	{
		$returndata = array();
		$users = new Users();
		$user_id = $this->request->getPost('user_id');
		$full_name = $this->request->getPost('full_name');
		$email = $this->request->getPost('email');
		$mobile = $this->request->getPost('mobile');
		$profile_image = $this->request->getFile("profile_image");
			
		if($profile_image){
			if($profile_image->isValid()){
				$photopath = $profile_image->store();
				if($photopath != ""){
					$users->crud_update(array(
						'full_name' => $full_name,
						'user_email' => $email,
						'mobile' => $mobile,
						'is_social' => '',
						'profile_image' => $photopath
					), $user_id);
				}
			}
		}
		else {
			$users->crud_update(array(
				'full_name' => $full_name,
				'user_email' => $email,
				'mobile' => $mobile,
				'is_social' => '',
			), $user_id);
		}
		$returndata['status'] = TRUE;
		$returndata['message'] = "User profile updated successfully.";
		
		return $this->apiResponse($returndata);
	}

	public function save_recent_play_song()
	{
		$returndata = array();
		$songs = new Songs();
		$user_id = $this->request->getPost('user_id');
		$song_id = $this->request->getPost('song_id');
		$recent_play_song_details = $songs->read_recent_song($user_id);
		if(count($recent_play_song_details) > 10){
			$songs->delete_recent_song($recent_play_song_details[9]['recent_played_song_id']);
			$songs->create_recent_song(array(
				'userid' => $user_id,
				'songid' => $song_id,
				'created_at' => date('Y-m-d H:i:s')
			));
		}
		else {
			$songs->create_recent_song(array(
				'userid' => $user_id,
				'songid' => $song_id,
				'created_at' => date('Y-m-d H:i:s')
			));
		}
		$returndata['status'] = true;
    	$returndata['message'] = "Song saved into recent playlist";
		return $this->apiResponse($returndata);
	}

	public function get_recent_play_song()
	{
		$returndata = array();
		$songs = new Songs();
		$user_id = $this->request->getPost('user_id');
		if(!empty($user_id)){
			$recent_play_song_details = $songs->read_recent_song($user_id);
			foreach ($recent_play_song_details as $recent_songs_data) {
				$songs_details = $songs->crud_read($recent_songs_data['songid']);
				$returndata['data'][] = array(
					'songs_id' => $songs_details[0]['songs_id'],
					'song_name' => $songs_details[0]['song_name'],
					'thumbnail' => base_url()."/writable/uploads/".$songs_details[0]['thumbnail'],
					'song_artist' => $songs_details[0]['song_artist'],
					'song_track' => base_url()."/writable/uploads/".$songs_details[0]['song_track'],
					'created_at' => date('m-d-Y', strtotime($songs_details[0]['created_at']))
				);
			}
			$returndata['status'] = true;
	    	$returndata['message'] = "Data fetched successfully";
		}
		else {
			$returndata['status'] = false; 
            $returndata['message'] = 'Please provide required fields.' ;
		}
		return $this->apiResponse($returndata);
	}

	public function get_home_data()
	{
		$returndata = array();
		$artist = new Artist();
		$songs = new Songs();
		$user_id = $this->request->getPost('user_id');

		$songs_details = $songs->crud_read();
		$artist_details = $artist->crud_read();
		$trending_song_details = $songs->count_play_song();
		$top_10_songs_details = $songs->count_like_song();
		$recent_play_song_details = $songs->read_recent_song($user_id);

		$returndata['artistsData'] = array();
		foreach ($artist_details as $artist_data) {
			$returndata['artistsData'][] = array(
				'artist_id' => $artist_data['artist_id'],
				'artist_name' => $artist_data['artist_name'],
				'banner_image' => base_url()."/writable/uploads/".$artist_data['banner_image'],
				'cover_image' => base_url()."/writable/uploads/".$artist_data['cover_image'],
				'description' => $artist_data['description'],
				//'top_10' => $artist_data['top_10'],
				'created_at' => date('m-d-Y', strtotime($artist_data['created_at']))
			);
		}

		$returndata['songsData'] = array();
		foreach ($songs_details as $songs_data) {
			$returndata['songsData'][] = array(
				'songs_id' => $songs_data['songs_id'],
				'song_name' => $songs_data['song_name'],
				'thumbnail' => base_url()."/writable/uploads/".$songs_data['thumbnail'],
				'song_artist' => $songs_data['song_artist'],
				'song_track' => base_url()."/writable/uploads/".$songs_data['song_track'],
				'created_at' => date('m-d-Y', strtotime($songs_data['created_at']))
			);
		}

		$returndata['recentPlayData'] = array();
		foreach ($recent_play_song_details as $recent_songs_data) {
			$songs_details2 = $songs->crud_read($recent_songs_data['songid']);
			$returndata['recentPlayData'][] = array(
				'songs_id' => $songs_details2[0]['songs_id'],
				'song_name' => $songs_details2[0]['song_name'],
				'thumbnail' => base_url()."/writable/uploads/".$songs_details2[0]['thumbnail'],
				'song_artist' => $songs_details2[0]['song_artist'],
				'song_track' => base_url()."/writable/uploads/".$songs_details2[0]['song_track'],
				'created_at' => date('m-d-Y', strtotime($recent_songs_data['created_at']))
			);
		}

		$returndata['recommendData'] = array();
		foreach ($recent_play_song_details as $recent_songs_data) {
			$songs_details2 = $songs->crud_read($recent_songs_data['songid']);
			$recommend_artist_details = $artist->search_artist($songs_details2[0]['song_artist']);
			foreach ($recommend_artist_details as $recommend_artist) {
				$returndata['recommendData'][] = array(
					'artist_id' => $recommend_artist['artist_id'],
					'artist_name' => $recommend_artist['artist_name'],
					'banner_image' => base_url()."/writable/uploads/".$recommend_artist['banner_image'],
					'cover_image' => base_url()."/writable/uploads/".$recommend_artist['cover_image'],
					'description' => $recommend_artist['description'],
					'created_at' => date('m-d-Y', strtotime($recommend_artist['created_at']))
				);
			}
		}

		$returndata['trendingSongsData'] = array();
		uasort($trending_song_details, function ($i, $j) {
		    $a = $i['trending_song_order'];
		    $b = $j['trending_song_order'];
		    if ($a == $b) return 0;
		    elseif ($a > $b) return 1;
		    else return -1;
		});
		foreach ($trending_song_details as $trending_songs_data) {
			$returndata['trendingSongsData'][] = array(
				'songs_id' => $trending_songs_data['songs_id'],
				'song_name' => $trending_songs_data['song_name'],
				'song_artist' => $trending_songs_data['song_artist'],
				'thumbnail' => base_url()."/writable/uploads/".$trending_songs_data['thumbnail'],
				'song_track' => base_url()."/writable/uploads/".$trending_songs_data['song_track'],
				'created_at' => date('m-d-Y', strtotime($trending_songs_data['created_at']))
			);
		}

		$returndata['top10SongsData'] = array();
		uasort($top_10_songs_details, function ($i, $j) {
		    $a = $i['top10_song_order'];
		    $b = $j['top10_song_order'];
		    if ($a == $b) return 0;
		    elseif ($a > $b) return 1;
		    else return -1;
		});
		foreach ($top_10_songs_details as $top_10_song_data) {
				$returndata['top10SongsData'][] = array(
					'songs_id' => $top_10_song_data['songs_id'],
					'song_name' => $top_10_song_data['song_name'],
					'song_artist' => $top_10_song_data['song_artist'],
					'thumbnail' => base_url()."/writable/uploads/".$top_10_song_data['thumbnail'],
					'song_track' => base_url()."/writable/uploads/".$top_10_song_data['song_track'],
					'created_at' => date('m-d-Y', strtotime($top_10_song_data['created_at']))
				);
			}
		$returndata['status'] = true;
    	$returndata['message'] = "Home data fetched successfully!";
		return $this->apiResponse($returndata);
	}

	public function search_playlists_song()
	{
		$returndata = array();
		$playlist_songs = new Playlist_songs();
		$search_text = $this->request->getPost('search_text');
		$status = $this->request->getPost('status');
		if(!empty($search_text)){	
			$songs_details = $playlist_songs->search_playlist_songs($search_text, $status);
			if(!empty($songs_details)){
				$song_array = array();
				foreach ($songs_details as $song) {
					$song_array[] = array(
						'playlist_song_id' => $song['playlist_song_id'],
						'song_name' => $song['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$song['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$song['song_track'],
						'favorite' => $song['favorite'],
						'created_at' => date('m-d-Y', strtotime($song['created_at']))
					);
				}
				$returndata['status'] = TRUE;
				$returndata['data'] = $song_array;
				$returndata['message'] = "Search data fetched successfully.";
			}
			else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";	
		}
		return $this->apiResponse($returndata);
	}

	public function search_artist()
	{
		$returndata = array();
		$artist = new Artist();
		$search_text = $this->request->getPost('search_text');
		if(!empty($search_text)){	
			$artist_details = $artist->search_artist($search_text);
			if($artist_details){
				$artist_array = array();
				foreach ($artist_details as $artist_data) {
					$artist_array[] = array(
						'artist_id' => $artist_data['artist_id'],
						'artist_name' => $artist_data['artist_name'],
						'banner_image' => base_url()."/writable/uploads/".$artist_data['banner_image'],
						'cover_image' => base_url()."/writable/uploads/".$artist_data['cover_image'],
						'description' => $artist_data['description'],
						//'top_10' => $artist_data['top_10'],
						'created_at' => date('m-d-Y', strtotime($artist_data['created_at']))
					);
				}
				$returndata['status'] = TRUE;
				$returndata['data'] = $artist_array;
				$returndata['message'] = "Artists fetched successfully.";
			}
			else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";	
		}
		return $this->apiResponse($returndata);
	}

	public function home_search()
	{
		$returndata = array();
		$artist = new Artist();
		$playlist_songs = new Playlist_songs();
		$search_text = $this->request->getPost('search_text');
		if(!empty($search_text)){	
			$artist_details = $artist->search_artist($search_text);
			$songs_details = $playlist_songs->search_playlist_songs($search_text);
			if(!empty($artist_details)){
				$data_array = array();
				foreach ($artist_details as $artist_data) {
					$data_array[] = array(
						'artist_id' => $artist_data['artist_id'],
						'artist_name' => $artist_data['artist_name'],
						'banner_image' => base_url()."/writable/uploads/".$artist_data['banner_image'],
						'cover_image' => base_url()."/writable/uploads/".$artist_data['cover_image'],
						'description' => $artist_data['description'],
						//'top_10' => $artist_data['top_10'],
						'created_at' => date('m-d-Y', strtotime($artist_data['created_at']))
					);
				}
				$returndata['status'] = TRUE;
				$returndata['data'] = $data_array;
				$returndata['message'] = "Data fetched successfully.";
			}
			elseif(!empty($songs_details)){
				$data_array = array();
				foreach ($songs_details as $song) {
					$data_array[] = array(
						'playlist_song_id' => $song['playlist_song_id'],
						'song_name' => $song['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$song['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$song['song_track'],
						'favorite' => $song['favorite'],
						'created_at' => date('m-d-Y', strtotime($song['created_at']))
					);
				}
				$returndata['status'] = TRUE;
				$returndata['data'] = $data_array;
				$returndata['message'] = "Search data fetched successfully.";
			}
			else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";	
		}
		return $this->apiResponse($returndata);
	}

	public function save_recent_search()
	{
        $returndata = array();
		$recent_search = new Recent_search();
		$search_text = $this->request->getPost('search_text');
        $user_id = $this->request->getPost('user_id');
        $artist_id = $this->request->getPost('artist_id');
        $playlist_song_id = $this->request->getPost('playlist_song_id');
        if (!empty($search_text) && !empty($user_id)){
        	if(!empty($artist_id)){
        		$insert = $recent_search->crud_create(array(
	                'search_text' => $search_text,
	                'userid' => $user_id,
	                'artist_id' => $artist_id,
	                'created_at' => date('Y-m-d H:i:s')
	            ));
        	}
        	else {
        		$insert = $recent_search->crud_create(array(
	                'search_text' => $search_text,
	                'userid' => $user_id,
	                'playlist_song_id' => $playlist_song_id,
	                'created_at' => date('Y-m-d H:i:s')
	            ));
        	}
            $returndata['status'] = true;                                   
            $returndata['message'] = 'Recent search saved successfully.';
        } else {
            $returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
        }
        return $this->apiResponse($returndata);
    }

    public function get_recent_search()
    {
        $returndata = array();
		$recent_search = new Recent_search();
		$user_id = $this->request->getPost('user_id');
		if (!empty($user_id)){
			$recent_details = $recent_search->crud_read($user_id);
			//echo $recent_search->getLastQuery();
			if(!empty($recent_details)){
				$returndata['status'] = TRUE;
				$returndata['data'] = $recent_details;
				$returndata['message'] = "Recent search fetched successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else {
            $returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
        }
		return $this->apiResponse($returndata);
	}

	public function set_trending_song()
	{
		$returndata = array();
		$songs = new Songs();
		$trending_songs = new Trending_songs();
		$song_id = $this->request->getPost('song_id');
		$user_id = $this->request->getPost('user_id');
		if(!empty($song_id) && !empty($user_id)){
			$trending_song_detail = $trending_songs->crud_read($user_id, $song_id);
			//echo $trending_songs->getLastQuery();die;
			$song_detail = $songs->crud_read($song_id);
			if(!empty($trending_song_detail)){
				$trending_songs->crud_update(array(
	                'userid' => $user_id,
	                'songid' => $song_id,
	                'play_times' => $trending_song_detail[0]['play_times'] + 1,
	                'created_at' => date('Y-m-d H:i:s')
	            ), $trending_song_detail[0]['trending_song_id']);
	            $songs->crud_update(array(
					'play_count' => $song_detail[0]['play_count']+1
				), $song_id);
				$returndata['status'] = TRUE;                   
            	$returndata['message'] = 'you play already played song again.';
			}
			else {
				$trending_songs->crud_create(array(
	                'userid' => $user_id,
	                'songid' => $song_id,
	                'play_times' => 1,
	                'created_at' => date('Y-m-d H:i:s')
	            ));
	            $songs->crud_update(array(
					'play_count' => $song_detail[0]['play_count']+1
				), $song_id);
				$returndata['status'] = TRUE;                   
            	$returndata['message'] = 'You played song.';
			}
		}
		else {
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function get_trending_songs()
    {
        $returndata = array();
		$trending_songs = new Songs();
		$trending_song_details = $trending_songs->count_play_song();
		if(!empty($trending_song_details)){
			uasort($trending_song_details, function ($i, $j) {
			    $a = $i['trending_song_order'];
			    $b = $j['trending_song_order'];
			    if ($a == $b) return 0;
			    elseif ($a > $b) return 1;
			    else return -1;
			});
			$data_array = array();
			foreach ($trending_songs_details as $song) {
				$data_array[] = array(
					'trending_song_id' => $song['trending_song_id'],
					'song_name' => $song['song_name'],
					'thumbnail' => base_url()."/writable/uploads/".$song['thumbnail'],
					'song_track' => base_url()."/writable/uploads/".$song['song_track'],
					'created_at' => date('m-d-Y', strtotime($song['created_at']))
				);
			}
			$returndata['status'] = TRUE;
			$returndata['data'] = $data_array;
			$returndata['message'] = "Trending songs fetched successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function set_top10_song()
	{
		$returndata = array();
		$songs = new Songs();
		$top_10_songs = new Top_10_songs();
		$song_id = $this->request->getPost('song_id');
		$user_id = $this->request->getPost('user_id');
		if(!empty($song_id) && !empty($user_id)){
			$top10_song_detail = $top_10_songs->crud_read($user_id, $song_id);
			$song_detail = $songs->crud_read($song_id);
			if(!empty($top10_song_detail)){
				$returndata['status'] = false;                   
            	$returndata['message'] = 'You have already liked this song.';
			}
			else {
				$top_10_songs->crud_create(array(
	                'userid' => $user_id,
	                'songid' => $song_id,
	                'created_at' => date('Y-m-d H:i:s')
	            ));
	            $songs->crud_update(array(
					'like_count' => $song_detail[0]['like_count']+1
				), $song_id);
				$returndata['status'] = TRUE;                   
            	$returndata['message'] = 'You liked this song.';
			}
		}
		else {
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function get_top10_songs()
    {
        $returndata = array();
		$songs = new Songs();
		$top10_song_details = $songs->count_like_song();
		if(!empty($top10_song_details)){
			uasort($top10_song_details, function ($i, $j) {
			    $a = $i['top10_song_order'];
			    $b = $j['top10_song_order'];
			    if ($a == $b) return 0;
			    elseif ($a > $b) return 1;
			    else return -1;
			});
			$data_array = array();
			foreach ($top10_song_details as $song) {
				$data_array[] = array(
					'songs_id' => $song['songs_id'],
					'song_name' => $song['song_name'],
					'thumbnail' => base_url()."/writable/uploads/".$song['thumbnail'],
					'song_track' => base_url()."/writable/uploads/".$song['song_track'],
					'created_at' => date('m-d-Y', strtotime($song['created_at']))
				);
			}
			$returndata['status'] = TRUE;
			$returndata['data'] = $data_array;
			$returndata['message'] = "Top 10 songs fetched successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_subscriptions()
    {
        $returndata = array();
		$subscription = new Subscription();
		$subscription_details = $subscription->crud_read();
		if(!empty($subscription_details)){
			$data_array = array();
			foreach ($subscription_details as $subs) {
				$data_array[] = array(
					'plan_id' => $subs['plan_id'],
					'plan_name' => $subs['plan_name'],
					'plan_amount' => $subs['plan_amount'],
					'plan_status' => $subs['plan_status'],
					'plan_description' => $subs['plan_description'],
					'created_at' => date('m-d-Y', strtotime($subs['created_at']))
				);
			}
			$returndata['status'] = TRUE;
			$returndata['data'] = $data_array;
			$returndata['message'] = "Subscription plan fetched successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_user_songs()
    {
        $returndata = array();
		$user_songs = new User_songs();
		$user_id = $this->request->getPost('user_id');
		$user_songs_details = $user_songs->crud_read('', $user_id);
		if(!empty($user_id)){
			if(!empty($user_songs_details)){
				$data_array = array();
				foreach ($user_songs_details as $song) {
					$data_array[] = array(
						'user_song_id' => $song['user_song_id'],
						'song_name' => $song['song_name'],
						'song_artist' => $song['song_artist'],
						//'top_10' => $song['top_10'],
						'thumbnail' => base_url()."/writable/uploads/".$song['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$song['song_track'],
						'created_at' => date('m-d-Y', strtotime($song['created_at']))
					);
				}
				$returndata['status'] = TRUE;
				$returndata['data'] = $data_array;
				$returndata['message'] = "User songs fetched successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else {
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function get_stations()
	{
		$returndata = array();
		$stations = new Stations();
		$station_details = $stations->crud_read();
		if(!empty($station_details)){
			$data_array = array();
			foreach ($station_details as $station) {
				$data_array[] = array(
					'station_id' => $station['station_id'],
					'station_name' => $station['station_name'],
					'banner_image' => base_url()."/writable/uploads/".$station['banner_image'],
					'created_at' => date('m-d-Y', strtotime($station['created_at']))
				);
			}
			$returndata['status'] = TRUE;
			$returndata['data'] = $data_array;
			$returndata['message'] = "Stations fetched successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_station_songs()
	{
		$returndata = array();
		$station_songs = new Station_songs();
		$station_id = $this->request->getPost('station_id');
		$station_song_details = $station_songs->crud_read('', $station_id);
		if(!empty($station_id)){
			if(!empty($station_song_details)){
				$data_array = array();
				foreach ($station_song_details as $song) {
					$data_array[] = array(
						'station_song_id' => $song['station_song_id'],
						'song_name' => $song['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$song['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$song['song_track'],
						'created_at' => date('m-d-Y', strtotime($song['created_at']))
					);
				}
				$returndata['status'] = TRUE;
				$returndata['data'] = $data_array;
				$returndata['message'] = "Station songs fetched successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else {
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function add_playlist_song_to_queue()
	{
		$returndata = array();
		$playlist_songs = new Playlist_songs();
		$user_id = $this->request->getPost('user_id');
		$song_id = $this->request->getPost('song_id');
		$song_type = $this->request->getPost('song_type'); //1- playlist, 2- artist song, 3- downloaded song
		if(!empty($user_id) && !empty($song_id) && !empty($song_type)){
			$exist = $playlist_songs->exist_queue($user_id, $song_id, $song_type);
			//echo $playlist_songs->getLastQuery();
			if(!empty($exist)){
				$returndata['status'] = FALSE;                   
        		$returndata['message'] = 'Song already added to queue.';
			}
			else {
				$playlist_songs->create_queue(array(
	                'userid' => $user_id,
	                'song_id' => $song_id,
	                'song_type' => $song_type,
	                'created_at' => date('Y-m-d H:i:s')
	            ));
	            $returndata['status'] = TRUE;                   
        		$returndata['message'] = 'Song added to queue.';
			}
		}
		else {
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function remove_playlist_song()
	{
		$returndata = array();
		$playlist_songs = new Playlist_songs();
		$playlist_song_id = $this->request->getPost('playlist_song_id');
		if(!empty($playlist_song_id)){
			$playlist_songs->crud_delete($playlist_song_id);
			$returndata['status'] = TRUE;                   
        	$returndata['message'] = 'Song removed from playlist.';
		}
		else {
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function get_playlist_song_from_queue()
	{
		$returndata = array();
		$playlist_songs = new Playlist_songs();
		$artist_songs = new Artist_songs();
		$download_song = new Download_song();
		$user_id = $this->request->getPost('user_id');
		if(!empty($user_id)){
			$queue_songs = $playlist_songs->read_user_queue($user_id);
			$data_array = array();
			foreach ($queue_songs as $queue) {
				$songs_array = array();
				if($queue['song_type'] == 1){
					$playlist_songs_details = $playlist_songs->crud_read($queue['song_id']);
					$songs_array[] = array(
						'playlist_song_id' => $playlist_songs_details[0]['playlist_song_id'],
						'playlistid' => $playlist_songs_details[0]['playlistid'],
						'song_name' => $playlist_songs_details[0]['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$playlist_songs_details[0]['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$playlist_songs_details[0]['song_track'],
						'favorite' => $playlist_songs_details[0]['favorite'],
						'created_at' => date('m-d-Y', strtotime($playlist_songs_details[0]['created_at']))
					);
				}

				if($queue['song_type'] == 2){
					$artist_song_details = $artist_songs->crud_read($queue['song_id']);
					$songs_array[] = array(
						'artist_song_id' => $artist_song_details[0]['artist_song_id'],
						'artistid' => $artist_song_details[0]['artistid'],
						'song_name' => $artist_song_details[0]['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$artist_song_details[0]['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$artist_song_details[0]['song_track'],
						'created_at' => date('m-d-Y', strtotime($artist_song_details[0]['created_at']))
					);
				}

				if($queue['song_type'] == 3){
					$download_song_details = $download_song->crud_read($queue['song_id']);
					if($download_song_details['song_type'] == 1){
						$song_details = $songs->crud_read($download_song_details['song_id']);
						$songs_array[] = array(
							'song_id' => $song_details[0]['songs_id'],
							'song_name' => $song_details[0]['song_name'],
							'thumbnail' => base_url()."/writable/uploads/".$song_details[0]['thumbnail'],
							'song_track' => base_url()."/writable/uploads/".$song_details[0]['song_track'],
							'song_artist' => $song_details[0]['song_artist'],
							'created_at' => date('m-d-Y', strtotime($song_details[0]['created_at']))
						);
					}
					elseif ($download_song_details['song_type'] == 2) {
						$playlist_songs_details2 = $playlist_songs->crud_read($download_song_details['song_id']);
						$songs_array[] = array(
							'playlist_song_id' => $playlist_songs_details2[0]['playlist_song_id'],
							'playlistid' => $playlist_songs_details2[0]['playlistid'],
							'song_name' => $playlist_songs_details2[0]['song_name'],
							'thumbnail' => base_url()."/writable/uploads/".$playlist_songs_details2[0]['thumbnail'],
							'song_track' => base_url()."/writable/uploads/".$playlist_songs_details2[0]['song_track'],
							'favorite' => $playlist_songs_details2[0]['favorite'],
							'created_at' => date('m-d-Y', strtotime($playlist_songs_details2[0]['created_at']))
						);
					}
				}
				$data_array[] = array(
					'playlist_song_queue_id' => $queue['playlist_song_queue_id'],
					'userid' => $queue['userid'],
					'songs' => $songs_array,
					'song_type' => $queue['song_type'],
					'created_at' => date('m-d-Y', strtotime($queue['created_at']))
				);
			}
			$returndata['status'] = TRUE;
			$returndata['data'] = $data_array;
			$returndata['message'] = "Queue fetched successfully.";
		}
		else {
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function download_song()
	{
		$returndata = array();
		$download_song = new Download_song();
		$user_id = $this->request->getPost('user_id');
		$song_id = $this->request->getPost('song_id');
		$song_type = $this->request->getPost('song_type');//1- song, 2- playlist song
		if(!empty($user_id) && (!empty($song_id) || !empty($song_type))){
			$exist = $download_song->exist_song($user_id, $song_id, $song_type);
			if(!empty($exist)){
				$returndata['status'] = FALSE;                   
        		$returndata['message'] = 'Song already downloaded.';
			}
			else {
				$download_song->crud_create(array(
					'userid' => $user_id,
	                'song_id' => $song_id,
	                'song_type' => $song_type,
	                'created_at' => date('Y-m-d H:i:s')
	            ));
	            $returndata['status'] = TRUE;                   
	    		$returndata['message'] = 'Song downloaded.';
	    	}
		}
		else {
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function delete_downloaded_song()
	{
		$returndata = array();
		$download_song = new Download_song();
		$downloaded_song_id = $this->request->getPost('downloaded_song_id');
		if(!empty($downloaded_song_id)){
			$download_song->crud_delete($downloaded_song_id);
            $returndata['status'] = TRUE;                   
    		$returndata['message'] = 'Song deleted.';
		}
		else {
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function view_downloaded_song()
	{
		$returndata = array();
		$download_song = new Download_song();
		$playlist_songs = new Playlist_songs();
		$Artist_songs = new Artist_songs();
		//$songs = new Songs();
		$user_id = $this->request->getPost('user_id');
		if(!empty($user_id)){
			$download_songs_details = $download_song->crud_read('', $user_id);
			
			$data_array = array();
			$count_all = count($download_songs_details);
			foreach ($download_songs_details as $downloaded) {
				$songs_array = array();
				if($downloaded['song_type'] == 1){
					
					$artist_song_details = $Artist_songs->crud_read($downloaded['song_id']);
					$songs_array[] = array(
						'artist_song_id' => $artist_song_details[0]['artist_song_id'],
						'song_name' => $artist_song_details[0]['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$artist_song_details[0]['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$artist_song_details[0]['song_track'],
						'created_at' => date('m-d-Y', strtotime($artist_song_details[0]['created_at']))
					);
				}
				
				if($downloaded['song_type'] == 2){
					$playlist_song_details = $playlist_songs->crud_read($downloaded['song_id']);
					$songs_array[] = array(
						'playlist_song_id' => $playlist_song_details[0]['playlist_song_id'],
						'playlistid' => $playlist_song_details[0]['playlistid'],
						'song_name' => $playlist_song_details[0]['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$playlist_song_details[0]['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$playlist_song_details[0]['song_track'],
						'favorite' => $playlist_song_details[0]['favorite'],
						'created_at' => date('m-d-Y', strtotime($playlist_song_details[0]['created_at']))
					);
				}
				
				$data_array[] = array(
					'downloaded_song_id' => $downloaded['downloaded_song_id'],
					'userid' => $downloaded['userid'],
					'songs' => $songs_array,
					'song_type' => $downloaded['song_type'],
					'created_at' => date('m-d-Y', strtotime($downloaded['created_at']))
				);
			}
			if(!empty($data_array)){
				$returndata['status'] = TRUE;
				$returndata['count'] = $count_all; 
	            $returndata['data'] = $data_array;
	    		$returndata['message'] = 'Downloaded songs list fetched.';
			}
           	else {
           		$returndata['status'] = false;                   
            	$returndata['message'] = 'No data found.';
           	}
		}
		else {
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function save_recent_play_station()
	{
		$returndata = array();
		$stations = new Stations();
		$user_id = $this->request->getPost('user_id');
		$station_id = $this->request->getPost('station_id');
		if(!empty($user_id) && !empty($station_id)){
			$exist = $stations->exist_recent_station($user_id, $station_id);
			if(!empty($exist)){
				$returndata['status'] = false;
	    		$returndata['message'] = "Already saved into recent play station.";
			}
			else {
				$stations->create_recent_station(array(
					'userid' => $user_id,
					'stationid' => $station_id,
					'created_at' => date('Y-m-d H:i:s')
				));
				$returndata['status'] = true;
	    		$returndata['message'] = "Saved into recent play station.";
			}
		}
		else {
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function radio_songs()
	{
		$returndata = array();
		$stations = new Stations();
		$user_id = $this->request->getPost('user_id');
		$station_details = $stations->crud_read();
		$recent_station_details = $stations->read_recent_station($user_id, 1);
		$top_station_details = $stations->read_max_station();
		/*echo $stations->getLastQuery();
		print_r($top_station_details);*/
		$returndata['stationsData'] = array();
		foreach ($station_details as $station) {
			$returndata['stationsData'][] = array(
				'station_id' => $station['station_id'],
				'station_name' => $station['station_name'],
				'banner_image' => base_url()."/writable/uploads/".$station['banner_image'],
				'created_at' => date('m-d-Y', strtotime($station['created_at']))
			);
		}

		$returndata['artistStationsData'] = array();
		foreach ($station_details as $station) {
			$returndata['artistStationsData'][] = array(
				'station_id' => $station['station_id'],
				'station_name' => $station['station_name'],
				'banner_image' => base_url()."/writable/uploads/".$station['banner_image'],
				'created_at' => date('m-d-Y', strtotime($station['created_at']))
			);
		}

		$returndata['recentStationsData'] = array();
		foreach ($recent_station_details as $recent_station) {
			$station_details2 = $stations->crud_read($recent_station['stationid']);
			$returndata['recentStationsData'][] = array(
				'station_id' => $station_details2[0]['station_id'],
				'station_name' => $station_details2[0]['station_name'],
				'banner_image' => base_url()."/writable/uploads/".$station_details2[0]['banner_image'],
				'created_at' => date('m-d-Y', strtotime($station_details2[0]['created_at']))
			);
		}

		$returndata['personalStationsData'] = array();
		foreach ($recent_station_details as $recent_station) {
			$station_details2 = $stations->crud_read($recent_station['stationid']);
			$returndata['personalStationsData'][] = array(
				'station_id' => $station_details2[0]['station_id'],
				'station_name' => $station_details2[0]['station_name'],
				'banner_image' => base_url()."/writable/uploads/".$station_details2[0]['banner_image'],
				'created_at' => date('m-d-Y', strtotime($station_details2[0]['created_at']))
			);
		}

		$recent_station_details2 = $stations->read_recent_station($user_id, 2);
		$returndata['recommendedStationsData'] = array();
		foreach ($recent_station_details2 as $recent_station2) {
			$station_details3 = $stations->crud_read($recent_station2['stationid']);
			$recommed_details = $stations->search_station_by_genre($station_details3[0]['station_genre']);
			foreach ($recommed_details as $recommed) {
				$data_array = array(
					'station_id' => $recommed['station_id'],
					'station_name' => $recommed['station_name'],
					'banner_image' => base_url()."/writable/uploads/".$recommed['banner_image'],
					'created_at' => date('m-d-Y', strtotime($recommed['created_at']))
				);
			}
			$returndata['recommendedStationsData'][] = $data_array;
		}

		$returndata['devotationalStationData'] = array();
		foreach ($station_details as $station) {
			if($station['station_name'] == 'Devotational station'){
				$returndata['devotationalStationData'][] = array(
					'station_id' => $station['station_id'],
					'station_name' => $station['station_name'],
					'banner_image' => base_url()."/writable/uploads/".$station['banner_image'],
					'created_at' => date('m-d-Y', strtotime($station['created_at']))
				);
			}
		}

		$returndata['topStationData'] = array();
		foreach ($top_station_details as $top_station) {
			$station_details4 = $stations->crud_read($top_station['stationid']);
			$returndata['topStationData'][] = array(
				'station_id' => $station_details4[0]['station_id'],
				'station_name' => $station_details4[0]['station_name'],
				'banner_image' => base_url()."/writable/uploads/".$station_details4[0]['banner_image'],
				'created_at' => date('m-d-Y', strtotime($station_details4[0]['created_at']))
			);
		}
			
		if(!empty($returndata['stationsData']) || !empty($returndata['artistStationsData']) || !empty($returndata['recentStationsData']) || !empty($returndata['personalStationsData']) || !empty($returndata['devotationalStationData']) || !empty($returndata['topStationData'])){
			$returndata['status'] = TRUE;
			$returndata['message'] = "Data fetched successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function radio_songs_by_type()
	{
		$returndata = array();
		$stations = new Stations();
		$user_id = $this->request->getPost('user_id');
		$type = $this->request->getPost('type');
		//1- station, 2-artistStations, 3-recentStations, 4-personalStations, 5-recommendedStations, 6- devotationalStation, 7- top chart
		$station_details = $stations->crud_read();
		$recent_station_details = $stations->read_recent_station($user_id, 1);
		$top_station_details = $stations->read_max_station();
		if($type == 1){
			foreach ($station_details as $station) {
				$returndata['data'][] = array(
					'station_id' => $station['station_id'],
					'station_name' => $station['station_name'],
					'banner_image' => base_url()."/writable/uploads/".$station['banner_image'],
					'created_at' => date('m-d-Y', strtotime($station['created_at']))
				);
			}
		}
		elseif ($type == 2) {
			foreach ($station_details as $station) {
				$returndata['data'][] = array(
					'station_id' => $station['station_id'],
					'station_name' => $station['station_name'],
					'banner_image' => base_url()."/writable/uploads/".$station['banner_image'],
					'created_at' => date('m-d-Y', strtotime($station['created_at']))
				);
			}
		}
		elseif ($type == 3) {
			foreach ($recent_station_details as $recent_station) {
				$station_details2 = $stations->crud_read($recent_station['stationid']);
				$returndata['data'][] = array(
					'station_id' => $station_details2[0]['station_id'],
					'station_name' => $station_details2[0]['station_name'],
					'banner_image' => base_url()."/writable/uploads/".$station_details2[0]['banner_image'],
					'created_at' => date('m-d-Y', strtotime($station_details2[0]['created_at']))
				);
			}
		}
		elseif ($type == 4) {
			foreach ($recent_station_details as $recent_station) {
				$station_details2 = $stations->crud_read($recent_station['stationid']);
				$returndata['data'][] = array(
					'station_id' => $station_details2[0]['station_id'],
					'station_name' => $station_details2[0]['station_name'],
					'banner_image' => base_url()."/writable/uploads/".$station_details2[0]['banner_image'],
					'created_at' => date('m-d-Y', strtotime($station_details2[0]['created_at']))
				);
			}
		}
		elseif ($type == 5) {
			$recent_station_details2 = $stations->read_recent_station($user_id, 2);
			foreach ($recent_station_details2 as $recent_station2) {
				$station_details3 = $stations->crud_read($recent_station2['stationid']);
				$recommed_details = $stations->search_station_by_genre($station_details3[0]['station_genre']);
				foreach ($recommed_details as $recommed) {
					$data_array = array(
						'station_id' => $recommed['station_id'],
						'station_name' => $recommed['station_name'],
						'banner_image' => base_url()."/writable/uploads/".$recommed['banner_image'],
						'created_at' => date('m-d-Y', strtotime($recommed['created_at']))
					);
				}
				$returndata['data'][] = $data_array;
			}
		}
		elseif ($type == 6) {
			foreach ($station_details as $station) {
				if($station['station_name'] == 'Devotational station'){
					$returndata['data'][] = array(
						'station_id' => $station['station_id'],
						'station_name' => $station['station_name'],
						'banner_image' => base_url()."/writable/uploads/".$station['banner_image'],
						'created_at' => date('m-d-Y', strtotime($station['created_at']))
					);
				}
			}
		}

		elseif ($type == 7) {
			foreach ($top_station_details as $top_station) {
				$station_details4 = $stations->crud_read($top_station['stationid']);
				$returndata['topStationData'][] = array(
					'station_id' => $station_details4[0]['station_id'],
					'station_name' => $station_details4[0]['station_name'],
					'banner_image' => base_url()."/writable/uploads/".$station_details4[0]['banner_image'],
					'created_at' => date('m-d-Y', strtotime($station_details4[0]['created_at']))
				);
			}
		}
	
		if(!empty($returndata['data'])){
			$returndata['status'] = TRUE;
			$returndata['message'] = "Data fetched successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_song_details()
	{
		$returndata = array();
		$songs = new Songs();
		$playlist_songs = new Playlist_songs();
		$station_songs = new Station_songs();
		$song_id = $this->request->getPost('song_id');
		$type = $this->request->getPost('type'); 
		// 1- song, 2- playlist song, 3- station song
		if(!empty($song_id) && !empty($type)){
			if($type == 1){
				$song_detail = $songs->crud_read($song_id);
				$returndata['data'][] = array(
					'song_id' => $song_detail[0]['songs_id'],
					'song_name' => $song_detail[0]['song_name'],
					'thumbnail' => base_url()."/writable/uploads/".$song_detail[0]['thumbnail'],
					'song_track' => base_url()."/writable/uploads/".$song_detail[0]['song_track'],
					'song_artist' => $song_detail[0]['song_artist'],
					'created_at' => date('m-d-Y', strtotime($song_detail[0]['created_at']))
				);
				$similar_songs_details = $songs->search_song_by_genre($song_detail[0]['song_genre'], $song_detail[0]['songs_id']);
				$data_array = array();
				foreach ($similar_songs_details as $similar) {
					$data_array[] = array(
						'song_id' => $similar['songs_id'],
						'song_name' => $similar['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$similar['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$similar['song_track'],
						'song_artist' => $similar['song_artist'],
						'created_at' => date('m-d-Y', strtotime($similar['created_at']))
					);
				}
				$returndata['similar_songs'] = $data_array;
			}
			elseif($type == 2) {
				$playlist_song_detail = $playlist_songs->crud_read($song_id);
				$returndata['data'][] = array(
					'playlist_song_id' => $playlist_song_detail[0]['playlist_song_id'],
					'playlistid' => $playlist_song_detail[0]['playlistid'],
					'song_name' => $playlist_song_detail[0]['song_name'],
					'thumbnail' => base_url()."/writable/uploads/".$playlist_song_detail[0]['thumbnail'],
					'song_track' => base_url()."/writable/uploads/".$playlist_song_detail[0]['song_track'],
					'favorite' => $playlist_song_detail[0]['favorite'],
					'created_at' => date('m-d-Y', strtotime($playlist_song_detail[0]['created_at']))
				);

				$similar_songs_details = $playlist_songs->search_song_by_genre($playlist_song_detail[0]['song_genre'], $playlist_song_detail[0]['playlist_song_id']);
				$data_array = array();
				foreach ($similar_songs_details as $similar) {
					$data_array[] = array(
						'playlist_song_id' => $similar['playlist_song_id'],
						'playlistid' => $similar['playlistid'],
						'song_name' => $similar['song_name'],
						'thumbnail' => base_url()."/writable/uploads/".$similar['thumbnail'],
						'song_track' => base_url()."/writable/uploads/".$similar['song_track'],
						'favorite' => $similar['favorite'],
						'created_at' => date('m-d-Y', strtotime($similar['created_at']))
					);
				}
				$returndata['similar_songs'] = $data_array;
			}
			elseif($type == 3) {
				$station_song_detail = $station_songs->crud_read($song_id);
				$returndata['data'][] = array(
					'station_song_id' => $station_song_detail[0]['station_song_id'],
					'song_name' => $station_song_detail[0]['song_name'],
					'thumbnail' => base_url()."/writable/uploads/".$station_song_detail[0]['thumbnail'],
					'song_track' => base_url()."/writable/uploads/".$station_song_detail[0]['song_track'],
					'created_at' => date('m-d-Y', strtotime($station_song_detail[0]['created_at']))
				);
				$returndata['similar_songs'] = [];
			}
			
			$returndata['status'] = TRUE;
			$returndata['message'] = "Data fetched successfully.";
		}
		else{
			$returndata['status'] = false;                   
            $returndata['message'] = 'Please provide required fields.';
		}
		return $this->apiResponse($returndata);
	}

	public function logout()
	{
		$returndata = array();
		$users = new Users();
		$user_id = $this->request->getPost("user_id");
		//$userdata = $users->crud_read($user_id);
		session_destroy();
            $returndata['status'] = true;                           
            $returndata['message'] = 'Logged out successfully.' ;
		/*if(count($userdata) < 1){
			$returndata['status'] = "error";
			$returndata['message'] = "Invalid User!";
			//return $this->respond($returndata,200);
		}
		$userdata = $userdata[0];
		if($type == 1){
			$users->crud_update(array("status" => 0),$user_id);
			$returndata['status'] = "success";
			$returndata['message'] = "Logged out successfully";
		}
		elseif($type == 2){
			$users->crud_delete($user_id);
			$returndata['status'] = "success";
			$returndata['message'] = "Account Deleted successfully";
		}
		else{
			$returndata['status'] = "error";
			$returndata['message'] = "Invalid Type!";
		}*/
		return $this->apiResponse($returndata);
	}
	
	
	public function get_faq()
	{
		
		$returndata = array();
		$faq = new Faq();
		$faq_detail = $faq->crud_read();
		//print_r($faq_detail);
		$returndata['faqs'] = $faq_detail;
		$returndata['status'] = TRUE;
		$returndata['message'] = "Data fetched successfully.";
		return $this->apiResponse($returndata);
	}

}
?>