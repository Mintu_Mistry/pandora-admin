<?php 
namespace App\Controllers;

/*-----Load models-----*/
use App\Models\Login;
use App\Models\Content;
use App\Models\Contact;
use App\Models\Feedback;
use App\Models\Users;
use App\Models\Artist;
use App\Models\Songs;
use App\Models\Artist_songs;
use App\Models\Artist_album;
use App\Models\User_playlist;
use App\Models\Playlist_songs;
use App\Models\Subscription;
use App\Models\User_songs;
use App\Models\Trending_songs;
use App\Models\Stations;
use App\Models\Station_songs;
use App\Models\Top_10_songs;
use App\Models\Artist_album_song;
use App\Models\Faq;

use CodeIgniter\HTTP\Files\UploadedFile;

class Admin extends BaseController
{

	public function __construct()
    {
        $this->session = session();
    }

	private function checkLogin()
	{
		if(!$this->session->get('admin_id')){
			header("location: ".base_url('admin')."/login");
		}
		else{
			return;
		}
	}

	private function loadView($viewname, $data = array())
	{
		echo view("admin/admin_header");
		echo view("admin/$viewname", $data);
		echo view("admin/admin_footer");
	}

	private function loadView1($viewname, $data = array())
	{
		echo view("admin/$viewname", $data);
	}

	public function index()
	{
		$this->checkLogin();
		$this->loadView("dashboard");	
	}

	public function credentialsVerify()
	{
		$login = new login();
		$admin_id = $login->admin_verify(
			$this->request->getPost('email'),
			$this->request->getPost('password')
		);
		if ($admin_id != NULL){
			$this->session->set('admin_id', $admin_id);
			$this->session->admin_data = $login->admin_data($admin_id);
		   	return redirect()->to(base_url('admin')); //dashboard
		}
		else{
			$data['msg'] = "Incorrect Email or Password";
			$this->loadView1("login", $data);
		}
	}

	public function logout()
	{
		$this->session->remove('admin_id');
		$this->session->destroy();
		return redirect()->to(base_url('admin/login') );
	}


	/*--------------------------Content Management----------------------------*/
	public function editContent($plan_id)
	{
		$this->checkLogin();
		$content = new Content();
		
		$content->crud_update(
			array(
				"page_name" => $this->request->getPost("page_name"),
				"page_content" => $this->request->getPost("page_content")
			), $plan_id
		);
		$this->session->setFlashdata('success_msg', "Content updated successfully");
		return redirect()->to(base_url('admin/content_management'));
	}
	
		/*--------------------------Faq Management----------------------------*/
	
	public function addFaq()
	{
		$this->checkLogin();
		$faq = new Faq();
		
		$faq->crud_create(
			array(
				"question" => $this->request->getPost("question"),
				"answer" => $this->request->getPost("answer")
			));
		$this->session->setFlashdata('success_msg', "Faq added successfully");
		return redirect()->to(base_url('admin/faq_management'));
	}
	
	public function editFaq($faqid)
	{
		$this->checkLogin();
		$faq = new Faq();
		
		$faq->crud_update(
			array(
				"question" => $this->request->getPost("question"),
				"answer" => $this->request->getPost("answer")
			), $faqid
		);
		$this->session->setFlashdata('success_msg', "Faq updated successfully");
		return redirect()->to(base_url('admin/faq_management'));
	}
	
	/*-------------------------- Users Management-------------------------*/
	public function addUser()
	{
		$this->checkLogin();
		$users = new Users();
		$full_name = $this->request->getPost("full_name");
		$user_email = $this->request->getPost("user_email");
		$mobile = $this->request->getPost("mobile");
		$profile_image = $this->request->getFile("profile_image");
		if($profile_image){
			if($profile_image->isValid()){
				$profile_imagepath = $profile_image->store();
				if($profile_imagepath != ""){
					$users->crud_create(
						array(
							"full_name" => $full_name,
							"user_email" => $user_email,
							"mobile" => $mobile,
							"profile_image" => $profile_imagepath,
							"created_at" => date('Y-m-d H:i:s')
						)
					);
				}
			}
		}
		$this->session->setFlashdata('success_msg', "User added successfully");
		return redirect()->to(base_url('admin/users_management'));
	}

	public function editUser($user_id)
	{
		$this->checkLogin();
		$users = new Users();
		$full_name = $this->request->getPost("full_name");
		$user_email = $this->request->getPost("user_email");
		$mobile = $this->request->getPost("mobile");
		$profile_image = $this->request->getFile("profile_image");
		if($profile_image->isValid()){
			$profile_imagepath = $profile_image->store();
			if($profile_imagepath != ""){
				$users->crud_update(
					array(
						"full_name" => $full_name,
						"user_email" => $user_email,
						"mobile" => $mobile,
						"profile_image" => $profile_imagepath,
						"created_at" => date('Y-m-d H:i:s')
					), $user_id
				);
			}
		}
		else {
			$users->crud_update(
				array(
					"full_name" => $full_name,
					"user_email" => $user_email,
					"mobile" => $mobile,
					"created_at" => date('Y-m-d H:i:s')
				), $user_id
			);
		}
		$this->session->setFlashdata('success_msg', "User updated successfully");
		return redirect()->to(base_url('admin/users_management'));
	}

	public function deleteUser($user_id)
	{
		$this->checkLogin();
		$users = new Users();
		$users->crud_delete($user_id);
		$this->session->setFlashdata('success_msg', "User deleted successfully");
		return redirect()->to(base_url('admin/users_management'));
	}


	/*-------------------------- Artist Management-------------------------*/
	public function addArtist()
	{
		$this->checkLogin();
		$artist = new Artist();
		$banner_image = $this->request->getFile("banner_image");
		$cover_image = $this->request->getFile("cover_image");
		$artist_name = $this->request->getPost("artist_name");
		$artist_genre = $this->request->getPost("artist_genre");
		$description = $this->request->getPost("description");
		if($banner_image && $cover_image){
			if($banner_image->isValid() && $cover_image->isValid()){
				$banner_imagepath = $banner_image->store();
				$cover_imagepath = $cover_image->store();
				if($banner_imagepath != "" && $cover_imagepath != ""){
					$artist->crud_create(
						array(
							"banner_image" => $banner_imagepath,
							"cover_image" => $cover_imagepath,
							"artist_name" => $artist_name,
							"artist_genre" => $artist_genre,
							"description" => $description,
							"created_at" => date('Y-m-d H:i:s')
						)
					);
				}
			}
		}
		$this->session->setFlashdata('success_msg', "Artist added successfully");
		return redirect()->to(base_url('admin/artist_management'));
	}

	public function editArtist($artist_id)
	{
		$this->checkLogin();
		$artist = new Artist();
		$banner_image = $this->request->getFile("banner_image");
		$cover_image = $this->request->getFile("cover_image");
		$artist_name = $this->request->getPost("artist_name");
		$artist_genre = $this->request->getPost("artist_genre");
		$description = $this->request->getPost("description");
		if($banner_image->isValid() && $cover_image->isValid()){
			$banner_imagepath = $banner_image->store();
			$cover_imagepath = $cover_image->store();
			if($banner_imagepath != "" && $cover_imagepath != ""){
				$artist->crud_update(
					array(
						"banner_image" => $banner_imagepath,
						"cover_image" => $cover_imagepath,
						"artist_name" => $artist_name,
						"artist_genre" => $artist_genre,
						"description" => $description,
						"created_at" => date('Y-m-d H:i:s')
					), $artist_id
				);
			}
		}
		else {
			$artist->crud_update(
				array(
					"artist_name" => $artist_name,
					"artist_genre" => $artist_genre,
					"description" => $description,
					"created_at" => date('Y-m-d H:i:s')
				), $artist_id
			);
		}
		$this->session->setFlashdata('success_msg', "Artist updated successfully");
		return redirect()->to(base_url('admin/artist_management'));
	}

	public function deleteArtist($artist_id)
	{
		$this->checkLogin();
		$artist = new Artist();
		$artist->crud_delete($artist_id);
		$this->session->setFlashdata('success_msg', "Artist deleted successfully");
		return redirect()->to(base_url('admin/artist_management'));
	}

	/*---------------Song Management----------------------------*/
	public function addSong()
	{
		$this->checkLogin();
		$songs = new Songs();
		$sound_track = $this->request->getFile("song_track");
		$song_image = $this->request->getFile("song_image");
		//$top_10 = $this->request->getPost("top_10");
		//$count = count($songs->count_top10());
		/*if($count <= 10 || $top_10 == 0){*/
			if($sound_track->isValid()){
				$sound_trackpath = $sound_track->store();
				if($sound_trackpath != ""){
					$song_id = $songs->crud_create(
						array(
							"song_name" => $this->request->getPost("song_name"),
							"song_genre" => $this->request->getPost("song_genre"),
							"song_track" => $sound_trackpath,
							"song_artist" => $this->request->getPost("song_artist")
						)
					);
					if($song_image->isValid()){
						$sound_imagepath = $song_image->store();
						if($sound_imagepath != ""){
							$songs->crud_update(
								array(
									"thumbnail" => $sound_imagepath
								), $song_id
							);
							
						}
					}
				}
			}
			$this->session->setFlashdata('success_msg', "Song added successfully");
		/*}
		else {
			$this->session->setFlashdata('error_msg', "Top 10 Songs already added");
		}*/
		return redirect()->to(base_url('admin/songs_management'));
	}

	public function editSong($song_id)
	{
		$this->checkLogin();
		$songs = new Songs();
		//print_r($_POST);die;
		$sound_track = $this->request->getFile("song_track");
		$song_image = $this->request->getFile("song_image");
		//$top_10 = $this->request->getPost("top_10");
		//$count = count($songs->count_top10());
		/*if($count <= 10 || $top_10 == 0){*/
			if ($song_image->isValid() && $sound_track->isValid()) {
				$sound_imagepath = $song_image->store();
				if($sound_imagepath != ""){
					$songs->crud_update(
						array(
							"song_name" => $this->request->getPost("song_name"),
							"song_genre" => $this->request->getPost("song_genre"),
							"song_artist" => $this->request->getPost("song_artist"),
							"thumbnail" => $sound_imagepath
						), $song_id
					);
				}
				$sound_trackpath = $sound_track->store();
				if($sound_trackpath != ""){
					$songs->crud_update(
						array(
							"song_name" => $this->request->getPost("song_name"),
							"song_genre" => $this->request->getPost("song_genre"),
							"song_track" => $sound_trackpath,
							"song_artist" => $this->request->getPost("song_artist")
						), $song_id
					);
				}
			}
			elseif($sound_track->isValid()){
				$sound_trackpath = $sound_track->store();
				if($sound_trackpath != ""){
					$songs->crud_update(
						array(
							"song_name" => $this->request->getPost("song_name"),
							"song_genre" => $this->request->getPost("song_genre"),
							"song_track" => $sound_trackpath,
							"song_artist" => $this->request->getPost("song_artist")
						), $song_id
					);
				}
			}
			elseif($song_image->isValid()){
				$sound_imagepath = $song_image->store();
				if($sound_imagepath != ""){
					$songs->crud_update(
						array(
							"song_name" => $this->request->getPost("song_name"),
							"song_genre" => $this->request->getPost("song_genre"),
							"song_artist" => $this->request->getPost("song_artist"),
							"thumbnail" => $sound_imagepath
						), $song_id
					);
				}
			}
			else {
				$songs->crud_update(
					array(
						"song_name" => $this->request->getPost("song_name"),
						"song_genre" => $this->request->getPost("song_genre"),
						"song_artist" => $this->request->getPost("song_artist")
					), $song_id
				);
			}
			$this->session->setFlashdata('success_msg', "Song updated successfully");
		/*}
		else {
			$this->session->setFlashdata('error_msg', "Top 10 Songs already added");
		}*/
		return redirect()->to(base_url('admin/songs_management'));
	}

	public function deleteSong($song_id)
	{
		$this->checkLogin();
		$songs = new Songs();
		$songs->crud_delete($song_id);
		$this->session->setFlashdata('success_msg', "Song deleted successfully");
		return redirect()->to(base_url('admin/songs_management'));
	}

	/*--------------------- Artist Song Management----------------------------*/
	public function addArtistSong()
	{
		$this->checkLogin();
		$artist_songs = new Artist_songs();
		$artistid = $this->request->getPost("artistid");
		//$top_10 = $this->request->getPost("top_10");
		$sound_track = $this->request->getFile("song_track");
		$song_image = $this->request->getFile("song_image");
		//$count = count($artist_songs->count_top10());
		//if($count <= 10){
			if($sound_track->isValid()){
				$sound_trackpath = $sound_track->store();
				if($sound_trackpath != ""){
					$song_id = $artist_songs->crud_create(
						array(
							"artistid" => $artistid,
							"song_name" => $this->request->getPost("song_name"),
							"song_genre" => $this->request->getPost("song_genre"),
							"song_track" => $sound_trackpath,
						)
					);
					if($song_image->isValid()){
						$sound_imagepath = $song_image->store();
						if($sound_imagepath != ""){
							$artist_songs->crud_update(
								array(
									"thumbnail" => $sound_imagepath
								), $song_id
							);
							
						}
					}
				}
			}
			$this->session->setFlashdata('success_msg', "Artist song added successfully");
		/*}
		else {
			$this->session->setFlashdata('error_msg', "Top 10 Songs already added");
		}*/
		return redirect()->to(base_url('admin/artist_songs_management/'.$artistid));
	}

	public function deleteArtistSong($artist_song_id, $artistid)
	{
		$this->checkLogin();
		$artist_songs = new Artist_songs();
		$artist_songs->crud_delete($artist_song_id);
		$this->session->setFlashdata('success_msg', "Artist song deleted successfully");
		return redirect()->to(base_url('admin/artist_songs_management/'.$artistid));
	}

	/*--------------------- Artist Album Management----------------------------*/
	public function addArtistAlbum()
	{
		$this->checkLogin();
		$artist_album = new Artist_album();
		$artistid = $this->request->getPost("artistid");
		$album_name = $this->request->getPost("album_name");
		//$top_10 = $this->request->getPost("top_10");
		//$sound_track = $this->request->getFile("song_track");
		$thumbnail = $this->request->getFile("thumbnail");
		//$count = count($artist_album->count_top10());
		/*if($count <= 10 || $top_10 == 0){*/
			
			if($thumbnail->isValid()){
				$sound_imagepath = $thumbnail->store();
				if($sound_imagepath != ""){
					$artist_album->crud_create(
						array(
							"artistid" => $artistid,
							"album_name" => $album_name,
							"thumbnail" => $sound_imagepath
						)
					);
					
				}
			}
			
			$this->session->setFlashdata('success_msg', "Artist album added successfully");
		/*}
		else {
			$this->session->setFlashdata('error_msg', "Top 10 albums already added");
		}*/
		return redirect()->to(base_url('admin/artist_album_management/'.$artistid));
	}

	public function deleteArtistAlbum($artist_album_id, $artistid)
	{
		$this->checkLogin();
		$artist_album = new Artist_album();
		$artist_album->crud_delete($artist_album_id);
		$this->session->setFlashdata('success_msg', "Artist album deleted successfully");
		return redirect()->to(base_url('admin/artist_album_management/'.$artistid));
	}

	public function addArtistAlbumSong()
	{
		$this->checkLogin();
		$artist_album_song = new Artist_album_song();
		$artist_albumid = $this->request->getPost("artist_album_id");
		$song_name = $this->request->getPost("song_name");
		$sound_track = $this->request->getFile("song_track");
		$song_image = $this->request->getFile("song_image");
		if($sound_track->isValid()){
			$sound_trackpath = $sound_track->store();
			if($sound_trackpath != ""){
				$insert_id = $artist_album_song->crud_create(
					array(
						"artist_albumid" => $artist_albumid,
						"song_name" => $song_name,
						"song_track" => $sound_trackpath,
						"created_at" => date('Y-m-d H:i:s')
					)
				);
				if($song_image->isValid()){
					$sound_imagepath = $song_image->store();
					if($sound_imagepath != ""){
						$artist_album_song->crud_update(
							array(
								"thumbnail" => $sound_imagepath
							), $insert_id
						);
						
					}
				}
			}
		}
		$this->session->setFlashdata('success_msg', "Album Song added successfully");
		return redirect()->to(base_url('admin/artist_album_song_management/'.$artist_albumid));
	}

	public function deleteArtistAlbumSong($artist_album_songs_id, $artist_albumid)
	{
		$this->checkLogin();
		$artist_album_song = new Artist_album_song();
		$artist_album_song->crud_delete($artist_album_songs_id);
		$this->session->setFlashdata('success_msg', "Album song deleted successfully");
		return redirect()->to(base_url('admin/artist_album_song_management/'.$artist_albumid));
	}

	/*--------------------- User Playlist Management----------------------------*/
	public function addUserPlaylist()
	{
		$this->checkLogin();
		$user_playlist = new User_playlist();
		$user_id = $this->request->getPost("user_id");
		$playlist_name = $this->request->getPost("playlist_name");
		$banner_image = $this->request->getFile("banner_image");
		if($banner_image){
			if($banner_image->isValid()){
				$banner_imagepath = $banner_image->store();
				if($banner_imagepath != ""){
					$user_playlist->crud_create(
						array(
							"userid" => $user_id,
							"playlist_name" => $playlist_name,
							"banner_image" => $banner_imagepath,
							"created_at" => date('Y-m-d H:i:s')
						)
					);
				}
			}
		}
		$this->session->setFlashdata('success_msg', "User Playlist created successfully");
		return redirect()->to(base_url('admin/user_playlist_management/'.$user_id));
	}

	public function deleteUserPlaylist($user_playlist_id, $user_id)
	{
		$this->checkLogin();
		$user_playlist = new User_playlist();
		$user_playlist->crud_delete($user_playlist_id);
		$this->session->setFlashdata('success_msg', "Playlist deleted successfully");
		return redirect()->to(base_url('admin/user_playlist_management/'.$user_id));
	}

	/*--------------------- Playlist Song Management----------------------------*/
	public function addPlaylistSong()
	{
		$this->checkLogin();
		$playlist_songs = new Playlist_songs();
		$playlist_id = $this->request->getPost("playlist_id");
		$sound_track = $this->request->getFile("song_track");
		$song_image = $this->request->getFile("song_image");
		$favorite = $this->request->getPost("favorite");
		if($sound_track->isValid()){
			$sound_trackpath = $sound_track->store();
			if($sound_trackpath != ""){
				$insert_id = $playlist_songs->crud_create(
					array(
						"playlistid" => $playlist_id,
						"song_name" => $this->request->getPost("song_name"),
						"song_genre" => $this->request->getPost("song_genre"),
						"song_track" => $sound_trackpath,
						"favorite" => $favorite
					)
				);
				
				if($song_image->isValid()){
					$sound_imagepath = $song_image->store();
					if($sound_imagepath != ""){
						$playlist_songs->crud_update(
							array(
								"thumbnail" => $sound_imagepath
							), $insert_id
						);
						
					}
				}
			}
		}

		$this->session->setFlashdata('success_msg', "Playlist Song added successfully");
		return redirect()->to(base_url('admin/playlist_song_management/'.$playlist_id));
	}

	public function deletePlaylistSong($playlist_song_id, $playlist_id)
	{
		$this->checkLogin();
		$playlist_songs = new Playlist_songs();
		$playlist_songs->crud_delete($playlist_song_id);
		$this->session->setFlashdata('success_msg', "Playlist song deleted successfully");
		return redirect()->to(base_url('admin/playlist_song_management/'.$playlist_id));
	}

	/*---------------Subscription Management----------------------------*/
	public function addSubscription()
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$subscription->crud_create(
			array(
				"plan_name" => $this->request->getPost("plan_name"),
				"plan_amount" => $this->request->getPost("plan_amount"),
				"plan_description" => $this->request->getPost("plan_description"),
				"plan_status" => $this->request->getPost("plan_status"),
				"created_at" => date('Y-m-d H:i:s')
			)
		);
		$this->session->setFlashdata('success_msg', "Subscription added successfully");
		return redirect()->to(base_url('admin/subscription_management'));
	}

	public function editSubscription($plan_id)
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$subscription->crud_update(
			array(
				"plan_name" => $this->request->getPost("plan_name"),
				"plan_amount" => $this->request->getPost("plan_amount"),
				"plan_description" => $this->request->getPost("plan_description"),
				"plan_status" => $this->request->getPost("plan_status"),
				"created_at" => date('Y-m-d H:i:s')
			), $plan_id
		);
		$this->session->setFlashdata('success_msg', "Subscription updated successfully");
		return redirect()->to(base_url('admin/subscription_management'));
	}

	public function deleteSubscription($plan_id)
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$subscription->crud_delete($plan_id);
		$this->session->setFlashdata('success_msg', "Subscription deleted successfully");
		return redirect()->to(base_url('admin/subscription_management'));
	}

	/*--------------------- User Song Management----------------------------*/
	public function addUserSong()
	{
		$this->checkLogin();
		$user_songs = new User_songs();
		$user_id = $this->request->getPost("userid");
		$song_name = $this->request->getPost("song_name");
		$sound_track = $this->request->getFile("song_track");
		$song_image = $this->request->getFile("song_image");
		$top_10 = $this->request->getPost("top_10");
		$song_artist = $this->request->getPost("song_artist");
		if($sound_track->isValid()){
			$sound_trackpath = $sound_track->store();
			if($sound_trackpath != ""){
				$insert_id = $user_songs->crud_create(
					array(
						"userid" => $user_id,
						"song_name" => $song_name,
						"song_track" => $sound_trackpath,
						"song_artist" => $song_artist,
						"top_10" => $top_10,
						"created_at" => date('Y-m-d H:i:s')
					)
				);
				if($song_image->isValid()){
					$sound_imagepath = $song_image->store();
					if($sound_imagepath != ""){
						$user_songs->crud_update(
							array(
								"thumbnail" => $sound_imagepath
							), $insert_id
						);
						
					}
				}
			}
		}
		$this->session->setFlashdata('success_msg', "User Song added successfully");
		return redirect()->to(base_url('admin/user_songs_management/'.$user_id));
	}

	public function deleteUserSong($user_song_id, $user_id)
	{
		$this->checkLogin();
		$user_songs = new User_songs();
		$user_songs->crud_delete($user_song_id);
		$this->session->setFlashdata('success_msg', "User song deleted successfully");
		return redirect()->to(base_url('admin/user_songs_management/'.$user_id));
	}

	

	/*--------------------- Station Management----------------------------*/
	public function addStation()
	{
		$this->checkLogin();
		$stations = new Stations();
		$station_name = $this->request->getPost("station_name");
		$station_genre = $this->request->getFile("station_genre");
		$banner_image = $this->request->getFile("banner_image");
		if($banner_image){
			if($banner_image->isValid()){
				$banner_imagepath = $banner_image->store();
				if($banner_imagepath != ""){
					$stations->crud_create(
						array(
							"station_name" => $station_name,
							"station_genre" => $station_genre,
							"banner_image" => $banner_imagepath,
							"created_at" => date('Y-m-d H:i:s')
						)
					);
				}
			}
		}
		$this->session->setFlashdata('success_msg', "Station created successfully");
		return redirect()->to(base_url('admin/stations_management'));
	}

	public function deleteStation($station_id)
	{
		$this->checkLogin();
		$stations = new Stations();
		$stations->crud_delete($station_id);
		$this->session->setFlashdata('success_msg', "Station deleted successfully");
		return redirect()->to(base_url('admin/stations_management'));
	}

	/*--------------------- Station Song Management----------------------------*/
	public function addStationSong()
	{
		$this->checkLogin();
		$station_songs = new Station_songs();
		$station_id = $this->request->getPost("station_id");
		$song_name = $this->request->getPost("song_name");
		$sound_track = $this->request->getFile("song_track");
		$song_genre = $this->request->getFile("song_genre");
		$song_image = $this->request->getFile("song_image");
		if($sound_track->isValid()){
			$sound_trackpath = $sound_track->store();
			if($sound_trackpath != ""){
				$insert_id = $station_songs->crud_create(
					array(
						"stationid" => $station_id,
						"song_name" => $song_name,
						"song_genre" => $song_genre,
						"song_track" => $sound_trackpath,
						"created_at" => date('Y-m-d H:i:s')
					)
				);
				if($song_image->isValid()){
					$sound_imagepath = $song_image->store();
					if($sound_imagepath != ""){
						$station_songs->crud_update(
							array(
								"thumbnail" => $sound_imagepath
							), $insert_id
						);
						
					}
				}
			}
		}
		$this->session->setFlashdata('success_msg', "Station Song added successfully");
		return redirect()->to(base_url('admin/station_songs_management/'.$station_id));
	}

	public function deleteStationSong($station_song_id, $station_id)
	{
		$this->checkLogin();
		$station_songs = new Station_songs();
		$station_songs->crud_delete($station_song_id);
		$this->session->setFlashdata('success_msg', "Station song deleted successfully");
		return redirect()->to(base_url('admin/station_songs_management/'.$station_id));
	}

	/*--------------- Top 10 Song Management----------------------------*/
	public function updateTop10Order()
	{
		$this->checkLogin();
		$songs = new Songs();
		$songid_arr = $this->request->getPost("songid");
		$song_order = $this->request->getPost("song_order");
		$check_unique = count($song_order) > count(array_unique($song_order));
		if(empty($check_unique))
		{
			$song_details = $songs->crud_read();
			foreach ($song_details as $value) {
				$songs->update_order(array(
						"top10_song_order" => 0
					), $value['songs_id']);
			}
			$i =0 ;
			foreach($songid_arr as $songid)
			{
				$songs->update_order(array(
					"top10_song_order" => $song_order[$i]
				), $songid);
				$i++;
			} 
			$this->session->setFlashdata('success_msg', "Song Order updated successfully");
		} else{
			$this->session->setFlashdata('error_msg', "Duplicate order number is not allowed");
		}
		
		return redirect()->to(base_url('admin/top10_songs_management'));
	}

	/*--------------- Trending Song Management----------------------------*/
	public function updateTrendingOrder()
	{
		$this->checkLogin();
		$songs = new Songs();
		$songid_arr = $this->request->getPost("songid");
		$song_order = $this->request->getPost("song_order");
		$check_unique = count($song_order) > count(array_unique($song_order));
		if(empty($check_unique))
		{
			$song_details = $songs->crud_read();
			foreach ($song_details as $value) {
				$songs->update_order(array(
						"trending_song_order" => 0
					), $value['songs_id']);
			}
			$i =0 ;
			foreach($songid_arr as $songid)
			{
				$songs->update_order(array(
					"trending_song_order" => $song_order[$i]
				),$songid);
				$i++;
			} 
			$this->session->setFlashdata('success_msg', "Song Order updated successfully");
		} else{
			$this->session->setFlashdata('error_msg', "Duplicate order number is not allowed");
		}
		
		return redirect()->to(base_url('admin/trending_songs_management'));
	}

	/*--------------------- Export Contact Management----------------------------*/

	public function export_contact_csv()
	{
		$this->checkLogin();
		$contact = new Contact();
		$filename = "contact_query_data.csv";
		$fp = fopen('php://output', 'w');
		$contact_query = $contact->crud_read();
		$header = array(
					"User Name",
					"Email",
					"Message",
					"Date"
				);
		fputcsv($fp, $header);
		foreach ($contact_query as $query) {
			$line =	array(
					"User Name" => $query['user_name'],
					"Email" => $query['email'],
					"Message" => $query['message'],
					"Date" => date('m-d-Y', strtotime($query['created_at']))
				);
			fputcsv($fp, $line);
		}
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename= $filename");
		header("Content-Type: application/csv; ");
		fclose($fp);
	}

	/*--------------------------Load Views----------------------------*/
	public function login()
	{
		echo view("admin/login");
	}

	public function content_management()
	{
		$this->checkLogin();
		$content = new Content();
		$data['content_details'] = $content->crud_read();
		$this->loadView("content", $data);
	}

	public function contact_management()
	{
		$this->checkLogin();
		$contact = new Contact();
		$data['contact_query'] = $contact->crud_read();
		$this->loadView("contact", $data);
	}
	
	
	public function faq_management()
	{
		$this->checkLogin();
		$faq = new Faq();
		$data['faq_details'] = $faq->crud_read();
		$this->loadView("faq", $data);
	}
	
	public function feedback_management()
	{
		$this->checkLogin();
		$feedback = new Feedback();
		$data['feedback_details'] = $feedback->crud_read();
		$this->loadView("feedback_management", $data);
	}

	public function users_management()
	{
		$this->checkLogin();
		$users = new Users();
		$data['users_details'] = $users->crud_read();
		$this->loadView("users_management", $data);
	}

	public function subscription_management()
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$data['subscription_plan'] = $subscription->crud_read();
		$this->loadView("subscription_plan", $data);
	}

	public function artist_management()
	{
		$this->checkLogin();
		$artist = new Artist();
		$data['artist_details'] = $artist->crud_read();
		$this->loadView("artist_management", $data);
	}

	public function songs_management()
	{
		$this->checkLogin();
		$songs = new Songs();
		$data['song_details'] = $songs->crud_read();
		$this->loadView("song_management", $data);
	}

	public function artist_songs_management($artist_id)
	{
		$this->checkLogin();
		$artist_songs = new Artist_songs();
		$data['artist_id'] = $artist_id;
		$data['artist_song_details'] = $artist_songs->crud_read('', $artist_id);
		$this->loadView("artist_song_management", $data);
	}

	public function artist_album_management($artist_id)
	{
		$this->checkLogin();
		$artist_album = new Artist_album();
		$data['artist_id'] = $artist_id;
		$data['artist_album_details'] = $artist_album->crud_read('', $artist_id);
		$this->loadView("artist_album_management", $data);
	}

	public function artist_album_song_management($artist_album_id)
	{
		$this->checkLogin();
		$artist_album_song = new Artist_album_song();
		$data['artist_album_id'] = $artist_album_id;
		$data['artist_album_song_details'] = $artist_album_song->crud_read('', $artist_album_id);
		$this->loadView("artist_album_song_management", $data);
	}

	public function user_playlist_management($user_id){
		$this->checkLogin();
		$user_playlist = new User_playlist();
		$data['user_id'] = $user_id;
		$data['playlist_details'] = $user_playlist->crud_read('', $user_id);
		$this->loadView("user_playlist", $data);
	}

	public function playlist_song_management($playlistid){
		$this->checkLogin();
		$playlist_songs = new Playlist_songs();
		$data['playlist_id'] = $playlistid;
		$data['playlist_song_details'] = $playlist_songs->crud_read('', $playlistid);
		$this->loadView("playlist_songs", $data);
	}

	public function user_songs_management($user_id)
	{
		$this->checkLogin();
		$user_songs = new User_songs();
		$data['user_id'] = $user_id;
		$data['user_song_details'] = $user_songs->crud_read('', $user_id);
		$this->loadView("user_song_management", $data);
	}

	public function stations_management()
	{
		$this->checkLogin();
		$stations = new Stations();
		$data['stations_details'] = $stations->crud_read();
		$this->loadView("station_management", $data);
	}

	public function station_songs_management($station_id)
	{
		$this->checkLogin();
		$station_songs = new Station_songs();
		$data['station_id'] = $station_id;
		$data['station_songs_details'] = $station_songs->crud_read('', $station_id);
		$this->loadView("station_song_management", $data);
	}

	public function top10_songs_management()
	{
		$this->checkLogin();
		$songs = new Songs();
		$data['top10_song_details'] = $songs->count_like_song();
		$this->loadView("top10_song_management", $data);
	}

	public function trending_songs_management()
	{
		$this->checkLogin();
		$trending_songs = new Songs();
		$data['trending_song_details'] = $trending_songs->count_play_song();
		$this->loadView("trending_song_management", $data);
	}
}
