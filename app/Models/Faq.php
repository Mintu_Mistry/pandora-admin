<?php  

namespace App\Models;

use CodeIgniter\Model;

class Faq extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('faq');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read()
	{	
		$this->builder->orderBy('faq_id', 'DESC');
		return $this->builder->get()->getResultArray();	
		
	}

	public function crud_update($data, $faq_id)
	{	
		$this->builder->where("faq_id",$faq_id);
		$this->builder->update($data);
	}

	/* public function crud_delete($artist_song_id)
	{	
		$this->builder->where('artist_song_id', $artist_song_id);
		$this->builder->delete();
	} */

}


?>