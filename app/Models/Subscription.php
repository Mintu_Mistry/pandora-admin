<?php  

namespace App\Models;

use CodeIgniter\Model;

class Subscription extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('subscription_plan');
    }
	
	function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}
	
	function crud_read($plan_id = '')
	{	
		if($plan_id > 0){
			$this->builder->where("plan_id",$plan_id);
			return $this->builder->get()->getResultArray();
		}
		else
			$this->builder->orderBy("plan_id","DESC");
			return $this->builder->get()->getResultArray();		
	}

	function crud_update($data, $plan_id)
	{	
		$this->builder->where("plan_id",$plan_id);
		$this->builder->update($data);
	}

	public function crud_delete($plan_id)
	{	
		$this->builder->where('plan_id', $plan_id);
		$this->builder->delete();
	}
}


?>