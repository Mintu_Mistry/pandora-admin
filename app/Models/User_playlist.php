<?php  

namespace App\Models;

use CodeIgniter\Model;

class User_playlist extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('user_playlist');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($user_playlist_id = '', $userid = '')
	{	
		if($user_playlist_id){
			$this->builder->where('user_playlist_id', $user_playlist_id);
			return $this->builder->get()->getResultArray();
		}
		elseif($userid){
			$this->builder->where('userid', $userid);
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('user_playlist_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $user_playlist_id)
	{	
		$this->builder->where("user_playlist_id",$user_playlist_id);
		$this->builder->update($data);
	}

	public function crud_delete($user_playlist_id)
	{	
		$this->builder->where('user_playlist_id', $user_playlist_id);
		$this->builder->delete();
	}
}


?>