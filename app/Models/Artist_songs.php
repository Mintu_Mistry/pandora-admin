<?php  

namespace App\Models;

use CodeIgniter\Model;

class Artist_songs extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('artist_songs_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read($artist_song_id = '', $artist_id = '')
	{	
		if($artist_song_id){
			$this->builder->where("artist_song_id",$artist_song_id);
			$this->builder->orderBy('artist_song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
		elseif($artist_id) {
			$this->builder->where("artistid",$artist_id);
			$this->builder->orderBy('artist_song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('artist_song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $artist_song_id)
	{	
		$this->builder->where("artist_song_id",$artist_song_id);
		$this->builder->update($data);
	}

	public function crud_delete($artist_song_id)
	{	
		$this->builder->where('artist_song_id', $artist_song_id);
		$this->builder->delete();
	}

	public function count_top10()
	{
		$this->builder->where('top_10', 1);
		return $this->builder->get()->getResultArray();
	}
}


?>