<?php  

namespace App\Models;

use CodeIgniter\Model;

class Artist_album_song extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('artist_album_songs_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read($artist_album_songs_id = '', $artist_albumid = '')
	{	
		if($artist_album_songs_id){
			$this->builder->where("artist_album_songs_id",$artist_album_songs_id);
			return $this->builder->get()->getResultArray();
		}
		elseif($artist_albumid) {
			$this->builder->where("artist_albumid",$artist_albumid);
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('artist_album_songs_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $artist_album_songs_id)
	{	
		$this->builder->where("artist_album_songs_id",$artist_album_songs_id);
		$this->builder->update($data);
	}

	public function crud_delete($artist_album_songs_id)
	{	
		$this->builder->where('artist_album_songs_id', $artist_album_songs_id);
		$this->builder->delete();
	}

	
}


?>