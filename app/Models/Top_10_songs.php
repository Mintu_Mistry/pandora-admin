<?php  

namespace App\Models;

use CodeIgniter\Model;

class Top_10_songs extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('top10_songs_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read($userid = '', $songid = '')
	{	
		if(!empty($userid) && !empty($songid)){
			$this->builder->where('userid', $userid);
			$this->builder->where('songid', $songid);
			return $this->builder->get()->getResultArray();
		}
		else {
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $top10_song_id)
	{	
		$this->builder->where("top10_song_id",$top10_song_id);
		$this->builder->update($data);
	}

	public function crud_delete($top10_song_id)
	{	
		$this->builder->where('top10_song_id', $top10_song_id);
		$this->builder->delete();
	}


}


?>