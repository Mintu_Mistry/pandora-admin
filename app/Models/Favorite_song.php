<?php  

namespace App\Models;

use CodeIgniter\Model;

class Favorite_song extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('user_fav_songs');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read($user_id,$song_type='',$song_id='')
	{
		if($song_type=='' && $song_id=='')
		{
			$this->builder->where("userid",$user_id);
			$this->builder->orderBy('fav_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}else{
			$this->builder->where("userid",$user_id);
			$this->builder->where("song_type",$song_type);
			$this->builder->where("track_id",$song_id);
			return $this->builder->get()->getResultArray();
		}
		
	}
	
	public function crud_read_by_favid($user_id,$fav_id)
	{
		$this->builder->where("userid",$user_id);
		$this->builder->where("fav_id",$fav_id);
		return $this->builder->get()->getResultArray();
		
	}

	public function crud_update($data, $playlist_song_id)
	{	
		$this->builder->where("playlist_song_id",$playlist_song_id);
		$this->builder->update($data);
	}

	public function crud_delete($fav_id)
	{	
		$this->builder->where('fav_id', $fav_id);
		$this->builder->delete();
	}

/* 	public function search_playlist_songs($search_text, $status = '')
	{
		if(!empty($status)){
			$this->builder->where('favorite', 1);
			$this->builder->like('song_name', $search_text);
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->like('song_name', $search_text);
			return $this->builder->get()->getResultArray();
		}
	}

	public function read_user_song_limit($playlist_id)
	{
		$this->builder->where("playlistid", $playlist_id);
		$this->builder->orderBy('playlist_song_id', 'DESC');
		$this->builder->limit(5);
		return $this->builder->get()->getResultArray();
	}

	public function search_song_by_genre($genre, $id='')
	{
		$this->builder->where("playlist_song_id !=", $id);
		$this->builder->like("song_genre", $genre);
		return $this->builder->get()->getResultArray();
	}

	public function create_queue($data)
	{
		$this->builder2->insert($data);
		return $this->db->insertID();
	}

	public function delete_queue($playlist_song_id, $user_id)
	{	
		$this->builder2->where('playlist_songid', $playlist_song_id);
		$this->builder2->where('userid', $user_id);
		$this->builder2->delete();
	}

	public function exist_queue($user_id='', $song_id='', $song_type='')
	{
		$this->builder2->where("userid", $user_id);
		$this->builder2->where("song_id", $song_id);
		$this->builder2->where("song_type", $song_type);
		return $this->builder2->get()->getRowArray();
	}

	public function read_user_queue($user_id)
	{
		$this->builder2->where("userid", $user_id);
		return $this->builder2->get()->getResultArray();
	} */
}


?>