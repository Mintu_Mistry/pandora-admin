<?php  

namespace App\Models;

use CodeIgniter\Model;

class Stations extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('station_details');
   		$this->builder2 =  $this->db->table('recent_played_stations_details');
    }
	
	function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}
	
	function crud_read($station_id = '')
	{	
		if($station_id > 0){
			$this->builder->where("station_id",$station_id);
			return $this->builder->get()->getResultArray();
		}
		else
			$this->builder->orderBy("station_id","DESC");
			return $this->builder->get()->getResultArray();		
	}

	function crud_update($data, $station_id)
	{	
		$this->builder->where("station_id",$station_id);
		$this->builder->update($data);
	}

	public function crud_delete($station_id)
	{	
		$this->builder->where('station_id', $station_id);
		$this->builder->delete();
	}

	public function search_station_by_genre($genre)
	{
		$this->builder->like("station_genre", $genre);
		return $this->builder->get()->getResultArray();
	}

	public function create_recent_station($data)
	{
		$this->builder2->insert($data);
		return $this->db->insertID();
	}

	public function read_recent_station($user_id = '', $type='')
	{
		if($type == 1){
			$this->builder2->where('userid', $user_id);
			$this->builder2->orderBy('created_at', 'DESC');
			$this->builder2->limit(1);
			return $this->builder2->get()->getResultArray();
		}
		else {
			$this->builder2->where('userid', $user_id);
			$this->builder2->orderBy('created_at', 'DESC');
			$this->builder2->limit(1);
			return $this->builder2->get()->getResultArray();
		}
		
	}

	public function exist_recent_station($user_id, $station_id)
	{
		$this->builder2->where("userid", $user_id);
		$this->builder2->where("stationid", $station_id);
		return $this->builder2->get()->getRowArray();
	}

	public function read_max_station()
	{
		$query = "SELECT *, COUNT(stationid) FROM recent_played_stations_details GROUP BY stationid ORDER BY COUNT(stationid) DESC LIMIT 5";
		return $this->db->query($query)->getResultArray();
	}

}


?>