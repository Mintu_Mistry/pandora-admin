<?php  

namespace App\Models;

use CodeIgniter\Model;

class Station_songs extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('station_songs_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read($station_song_id = '', $stationid = '')
	{	
		if($station_song_id){
			$this->builder->where("station_song_id",$station_song_id);
			$this->builder->orderBy('station_song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
		elseif($stationid) {
			$this->builder->where("stationid", $stationid);
			$this->builder->orderBy('station_song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('station_song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $station_song_id)
	{	
		$this->builder->where("station_song_id",$station_song_id);
		$this->builder->update($data);
	}

	public function crud_delete($station_song_id)
	{	
		$this->builder->where('station_song_id', $station_song_id);
		$this->builder->delete();
	}

}


?>