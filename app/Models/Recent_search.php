<?php  

namespace App\Models;

use CodeIgniter\Model;

class Recent_search extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('recent_search_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read($userid)
	{	
		$this->builder->where('userid',$userid);
		$this->builder->orderBy('recent_search_id', 'DESC');
		$this->builder->limit(5);
		return $this->builder->get()->getResultArray();
	}


}


?>