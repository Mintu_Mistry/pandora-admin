<?php  

namespace App\Models;

use CodeIgniter\Model;

class Songs extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('songs_details');
   		$this->builder2 =  $this->db->table('recent_played_songs_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read($songs_id = '')
	{
		if(!empty($songs_id)){
			$this->builder->where('songs_id', $songs_id);
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('songs_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $songs_id)
	{	
		$this->builder->where("songs_id",$songs_id);
		$this->builder->update($data);
	}

	public function crud_delete($songs_id)
	{	
		$this->builder->where('songs_id', $songs_id);
		$this->builder->delete();
	}

	public function count_top10()
	{
		$this->builder->where('top_10', 1);
		return $this->builder->get()->getResultArray();
	}

	public function count_like_song()
	{
		$this->builder->where('like_count !=', 0);
		$this->builder->orderBy('like_count', 'DESC');
		$this->builder->limit(10);
		return $this->builder->get()->getResultArray();
	}

	public function count_play_song()
	{
		$this->builder->where('play_count !=', 0);
		$this->builder->orderBy('play_count', 'DESC');
		$this->builder->limit(10);
		return $this->builder->get()->getResultArray();
	}

	public function update_order($data, $songs_id)
	{	
		$this->builder->where("songs_id",$songs_id);
		$this->builder->update($data);
	}

	public function search_song_by_genre($genre, $id='')
	{
		$this->builder->where("songs_id !=",$id);
		$this->builder->like("song_genre", $genre);
		return $this->builder->get()->getResultArray();
	}

	public function create_recent_song($data)
	{
		$this->builder2->insert($data);
		return $this->db->insertID();
	}

	public function read_recent_song($user_id = '')
	{
		$this->builder2->where('userid', $user_id);
		$this->builder2->orderBy('created_at', 'DESC');
		return $this->builder2->get()->getResultArray();
	}

	public function delete_recent_song($recent_played_song_id)
	{	
		$this->builder->where('recent_played_song_id', $recent_played_song_id);
		$this->builder->delete();
	}
}


?>