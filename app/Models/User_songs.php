<?php  

namespace App\Models;

use CodeIgniter\Model;

class User_songs extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('user_songs');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read($user_song_id = '', $userid = '')
	{	
		if($user_song_id){
			$this->builder->where("user_song_id",$user_song_id);
			$this->builder->orderBy('user_song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
		elseif($userid) {
			$this->builder->where("userid",$userid);
			$this->builder->orderBy('user_song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('user_song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $user_song_id)
	{	
		$this->builder->where("user_song_id",$user_song_id);
		$this->builder->update($data);
	}

	public function crud_delete($user_song_id)
	{	
		$this->builder->where('user_song_id', $user_song_id);
		$this->builder->delete();
	}

	
}


?>