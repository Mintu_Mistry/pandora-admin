<?php  

namespace App\Models;

use CodeIgniter\Model;

class Artist extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('artist_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($artist_id = '')
	{	
		if(!empty($artist_id)){
			$this->builder->where('artist_id', $artist_id);
			return $this->builder->get()->getRowArray();
		}
		else {
			$this->builder->orderBy('artist_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $artist_id)
	{	
		$this->builder->where("artist_id",$artist_id);
		$this->builder->update($data);
	}

	public function crud_delete($artist_id)
	{	
		$this->builder->where('artist_id', $artist_id);
		$this->builder->delete();
	}

	public function search_artist($search_text)
	{
		$this->builder->like('artist_name', $search_text);
		$this->builder->groupBy('artist_name');
		return $this->builder->get()->getResultArray();
	}

	public function search_artist_by_genre($genre, $artist_id = '')
	{
		if($artist_id){
			$this->builder->where('artist_id !=', $artist_id);
			$this->builder->like("artist_genre", $genre);
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->like("artist_genre", $genre);
			return $this->builder->get()->getResultArray();
		}
	}
}


?>