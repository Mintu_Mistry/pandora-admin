<?php  

namespace App\Models;

use CodeIgniter\Model;

class Download_song extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('downloaded_songs_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read($downloaded_song_id = '', $userid = '')
	{	
		if($downloaded_song_id){
			$this->builder->where("downloaded_song_id",$downloaded_song_id);
			$this->builder->orderBy('downloaded_song_id', 'DESC');
			return $this->builder->get()->getRowArray();
		}
		elseif($userid) {
			$this->builder->where("userid",$userid);
			$this->builder->orderBy('downloaded_song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('downloaded_song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $downloaded_song_id)
	{	
		$this->builder->where("downloaded_song_id",$downloaded_song_id);
		$this->builder->update($data);
	}

	public function crud_delete($downloaded_song_id)
	{	
		$this->builder->where('downloaded_song_id', $downloaded_song_id);
		$this->builder->delete();
	}

	public function exist_song($user_id, $song_id='', $song_type='')
	{
		$this->builder->where("userid", $user_id);
		$this->builder->where("song_id", $song_id);
		$this->builder->where("song_type", $song_type);
		return $this->builder->get()->getRowArray();
	}

	
}


?>