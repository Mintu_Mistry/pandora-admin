<?php  

namespace App\Models;

use CodeIgniter\Model;

class Trending_songs extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('trending_songs_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read($userid = '', $songid = '')
	{	
		if(!empty($userid) && !empty($songid)){
			$this->builder->where('userid', $userid);
			$this->builder->where('songid', $songid);
			return $this->builder->get()->getResultArray();
		}
		else {
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $trending_song_id)
	{	
		$this->builder->where("trending_song_id",$trending_song_id);
		$this->builder->update($data);
	}

	public function crud_delete($trending_song_id)
	{	
		$this->builder->where('trending_song_id', $trending_song_id);
		$this->builder->delete();
	}

	public function update_order($data, $songid)
	{	
		$this->builder->where("songid",$songid);
		$this->builder->update($data);
	}

	
}


?>