<?php  

namespace App\Models;

use CodeIgniter\Model;

class Feedback extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('feedback_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read()
	{	
		$this->builder->orderBy('feedback_id', 'DESC');
		return $this->builder->get()->getResultArray();
	}

	public function crud_update($data, $feedback_id )
	{	
		$this->builder->where("feedback_id",$feedback_id );
		$this->builder->update($data);
	}

	public function crud_delete($feedback_id)
	{	
		$this->builder->where('feedback_id', $feedback_id);
		$this->builder->delete();
	}
}


?>