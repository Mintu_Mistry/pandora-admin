<?php  

namespace App\Models;

use CodeIgniter\Model;

class Users extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('users_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	public function crud_read($user_id = '')
	{	
		if($user_id){
			$this->builder->where('user_id', $user_id);
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('user_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $user_id)
	{	
		$this->builder->where("user_id",$user_id);
		$this->builder->update($data);
	}

	public function crud_delete($user_id)
	{	
		$this->builder->where('user_id', $user_id);
		$this->builder->delete();
	}

	public function read_by_mobile($mobile = '')
	{	
		$this->builder->where('mobile', $mobile);
		return $this->builder->get()->getRowArray();
	}

	public function read_by_social_id($social_id = '')
	{	
		$this->builder->where('social_id', $social_id);
		return $this->builder->get()->getRowArray();
	}
}


?>